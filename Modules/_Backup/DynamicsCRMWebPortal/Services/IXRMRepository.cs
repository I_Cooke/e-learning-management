﻿using System; 
using System.Web;
using Microsoft.Xrm.Sdk;
using Xrm; 
using System.Collections.Generic;

namespace DynamicsCRMWebPortal.Services
{
    public interface IXrmRepository : IDisposable
    {
        bool IsDisposed();

        //me
        Guid RetrieveMyIdByIdentifier();
        Contact RetrieveMyInfoByIdentifier();
        void CreateOrUpdateMyInfo(Contact contact);

        //opportunities
        List<XrmRepository.OpportunityItem> RetrieveOpportunities(Guid contactId, string opportunityId);
        List<XrmRepository.OpportunityItem> ListOpportunities(List<Opportunity>opportunities);

        //incident
        List<XrmRepository.IncidentItem> RetrieveIncidents(Guid contactId, string ticketNumber);
        List<XrmRepository.IncidentItem> ListIncidents(List<Incident> incidents);

        //note
        List<XrmRepository.NotesInfo> RetrieveNotes(Guid objectId, Guid noteId);
        void CreateNote(string noteText, Guid objectId, string objectTypeCode, HttpPostedFileBase file);

        //article
        List<KbArticle> RetrieveArticlesBySubjectId(Guid subjectId);
        List<XrmRepository.ArticleItem> ListArticles(List<KbArticle> articles);

        //subject
        List<Subject> RetrieveSubjectByParent(string parentSubject);
        
        //resource
        List<Resource> RetrieveResourceByObjectType(string objectType);

        //appointment        
        List<ServiceAppointment> RetrieveAppointmentByParty(Guid partyId);
        List<String> RetrieveAppointmentOccupiedDateByResource(Guid partyId);
        

        //customer - contact
        List<Contact> RetrieveCustomers(Guid parentcustomerId, Guid contactId, bool inclMe);
        List<XrmRepository.ContactItem> ListCustomers(List<Contact> contacts);

        //order
        List<XrmRepository.OrderItem> RetrieveCustomerOrders(Guid parentcustomerId, string orderNumber);
        List<XrmRepository.OrderItem> ListOrders(List<SalesOrder> orders);

        //General
        void CreateCrmRecord(Entity entity);
        void UpdateCrmRecord(Entity entity);
        Entity RetrieveCrmRecord(string entityName, string attributeName, string attributeValue);
        List<Entity> RetrieveCrmRecords(string entityName, string attributeName, string attributeValue);
    }
}
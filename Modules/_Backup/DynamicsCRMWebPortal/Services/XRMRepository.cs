﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using DynamicsCRMWebPortal.Models;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Orchard;
using Orchard.ContentManagement;
using Xrm;

namespace DynamicsCRMWebPortal.Services
{
    public class XrmRepository : IXrmRepository
    {

        #region Set XRM context

        private readonly IOrchardServices _orchardServices;
        private readonly XrmServiceContext _xrm;
        private readonly string _identifier; //This is the read-only identifier for Orchard user, it can be ID or Email address.
        private const string CrmIdentifierField = "externaluseridentifier"; //This is a system attribute in the Contact entity, use it to store the Orchard _identifier.
        //private const string CrmIdentifierField = "new_portaluseridentifier"; //Alternatively, you can create a new attribute in CRM to store the Orchard _identifier.

        public XrmRepository(IOrchardServices orchardServices)
        {
            _orchardServices = orchardServices;

            try
            {
                if (_orchardServices != null && _orchardServices.WorkContext != null
                    && _orchardServices.WorkContext.CurrentUser != null && _orchardServices.WorkContext.CurrentUser.Id > 0)
                {

                    _identifier = _orchardServices.WorkContext.CurrentUser.Id.ToString(); //ID
                    //_identifier = _orchardServices.WorkContext.CurrentUser.Email.ToString(); //Email address

                    var crmSettings = orchardServices.WorkContext.CurrentSite.As<DynamicsCRMWebPortalSettingsPart>();
                    if (crmSettings != null && crmSettings.Record.CRMConnectionString != null)
                    {
                        var connection = CrmConnection.Parse(crmSettings.Record.CRMConnectionString);

                        _xrm = new XrmServiceContext(connection);
                        _xrm.TryAccessCache(cache => cache.Mode = OrganizationServiceCacheMode.InsertOnly);
                        _disposed = false;
                    }
                }
                else
                {
                    _disposed = true;
                }
            } 
            catch (OrchardException ex)
            {
                throw ex.InnerException;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region XRM Portal User - Contact

        //Retrieve contact by an identifier.
        public Contact RetrieveMyInfoByIdentifier()
        {
            try
            {
                var contacts = _xrm.ContactSet.Where(c => c[CrmIdentifierField] != null && c[CrmIdentifierField].Equals(_identifier));
                if (contacts != null && contacts.SingleOrDefault() != null)
                {
                    return contacts.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        //Retrieve contact id by an identifier.
        public Guid RetrieveMyIdByIdentifier()
        {
            try
            {
                var contact = _xrm.ContactSet.Where(c => c[CrmIdentifierField] != null && c[CrmIdentifierField].Equals(_identifier));
                if (contact != null)
                {
                    var singleOrDefault = contact.SingleOrDefault();
                    if (singleOrDefault != null && singleOrDefault.ContactId != null)
                    {
                        return singleOrDefault.ContactId.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            return Guid.Empty;
        }

        //Create or Update contact record.
        public void CreateOrUpdateMyInfo(Contact contact)
        {
            try
            {
                var crmContact = RetrieveMyInfoByIdentifier();

                if (crmContact == null) //contact not exist, then create.
                {
                    contact[CrmIdentifierField] = _identifier;

                    _xrm.AddObject(contact);
                    _xrm.SaveChanges();
                }
                else //contact exist, then update.
                {
                    foreach (var attribute in contact.Attributes)
                    {
                        if (attribute.Value != null)
                        {
                            crmContact[attribute.Key] = contact[attribute.Key];
                        }
                    }
                    _xrm.UpdateObject(crmContact);
                    _xrm.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        #endregion

        #region XRM Opportunity

        //Retrieve cases by opportunities
        public List<OpportunityItem> RetrieveOpportunities(Guid contactId, string opportunityId)
        {
            try
            {
                var opportunities = opportunityId == null ? _xrm.OpportunitySet.OrderByDescending(c => c.ModifiedOn).ToList() : _xrm.OpportunitySet.ToList();
                //var opportunities = opportunityId == null ? _xrm.OpportunitySet.Where(c => c.CustomerId.Id.Equals(contactId)).OrderByDescending(c => c.ModifiedOn).ToList() : _xrm.OpportunitySet.Where(c => (c.CustomerId.Id.Equals(contactId) && c.OpportunityId.Equals(opportunityId))).ToList();

                return ListOpportunities(opportunities);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
        public List<OpportunityItem> ListOpportunities(List<Opportunity> lstOpportunity)
        {
            var list = new List<OpportunityItem>();
            foreach (Opportunity opportunity in lstOpportunity)
            {
                list.Add(new OpportunityItem
                {
                    Name = opportunity.Name,
                    PurchaseTimeFrame = opportunity.PurchaseTimeframe != null ? opportunity.PurchaseTimeframe.Value.ToString() : string.Empty,
                    BudgetAmount = opportunity.BudgetAmount.ToString(),
                    PurchaseProcess = opportunity.PurchaseProcess != null ? opportunity.PurchaseProcess.Value.ToString() : string.Empty,
                    Description = opportunity.Description,
                    Id = opportunity.OpportunityId.Value.ToString(),
                    LastUpdate = opportunity.ModifiedOn.Value.ToLocalTime().ToString(CultureInfo.InvariantCulture)

                    //StatusCode = GetMetaLabel("opportunity", "statuscode", opportunity.StatusCode.Value, true),
                    //StateCode = GetMetaLabel("opportunity", "casetypecode", opportunity.StateCode.Value, false),

                });
            }
            return list;
        }

        public class OpportunityItem
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string PurchaseTimeFrame { get; set; }
            public string BudgetAmount { get; set; }
            public string PurchaseProcess { get; set; }
            public string Description { get; set; }
            public string LastUpdate { get; set; }

            //public string Title { get; set; }
            //public string StatusCode { get; set; }
            //public string StateCode { get; set; }
            //public string Description { get; set; }
            //public string LastUpdate { get; set; }
        }
        #endregion

        #region XRM Incident

        //Retrieve cases by contact
        public List<IncidentItem> RetrieveIncidents(Guid contactId, string ticketNumber)
        {
            try
            {
                var incidents = ticketNumber == null ? _xrm.IncidentSet.Where(c => c.CustomerId.Id.Equals(contactId)).OrderByDescending(c => c.ModifiedOn).ToList() : _xrm.IncidentSet.Where(c => (c.CustomerId.Id.Equals(contactId) && c.TicketNumber.Equals(ticketNumber))).ToList();

                return ListIncidents(incidents);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public List<IncidentItem> ListIncidents(List<Incident> incidents)
        {
            var list = new List<IncidentItem>();
            foreach (Incident incident in incidents)
            {
                list.Add(new IncidentItem
                {
                    Id = incident.IncidentId.Value.ToString(),
                    TicketNumber = incident.TicketNumber,
                    Title = incident.Title,
                    CaseStatus = GetMetaLabel("incident", "statuscode", incident.StatusCode.Value, true),
                    CaseType = GetMetaLabel("incident", "casetypecode", incident.CaseTypeCode.Value, false),
                    CaseStatusValue = incident.StatusCode.Value,
                    CaseTypeValue = incident.CaseTypeCode.Value,
                    Description = incident.Description != null ? incident.Description : string.Empty,
                    LastUpdate = incident.ModifiedOn.Value.ToLocalTime().ToString(CultureInfo.InvariantCulture)
                });
            }
            return list;
        }

        public class IncidentItem
        {
            public string Id { get; set; }
            public string TicketNumber { get; set; }
            public string Title { get; set; }
            public string CaseStatus { get; set; }
            public int CaseStatusValue { get; set; }
            public string CaseType { get; set; }
            public int CaseTypeValue { get; set; }
            public string Description { get; set; }
            public string LastUpdate { get; set; }
        }

        #endregion

        #region XRM Annotation

        //Create a note and link it to an entity
        public void CreateNote(string noteText, Guid objectId, string logicalName, HttpPostedFileBase file)
        {
            try
            {
                byte[] binaryData = null;

                if (file != null && file.ContentLength > 0)
                {
                    binaryData = new byte[file.InputStream.Length];
                    file.InputStream.Read(binaryData, 0, (int)file.InputStream.Length);
                    file.InputStream.Close();
                }

                var note = new Annotation
                {
                    NoteText = noteText,
                    ObjectId = new CrmEntityReference(logicalName, objectId),
                    FileName = (file != null ? Path.GetFileName(file.FileName) : null),
                    DocumentBody = (file != null && binaryData != null ? Convert.ToBase64String(binaryData) : null),
                    MimeType = "text/plain"
                };

                _xrm.AddObject(note);
                _xrm.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        //Retrieve notes by objectid
        public List<NotesInfo> RetrieveNotes(Guid objectId, Guid noteId)
        {
            try
            {
                var notes = noteId == Guid.Empty ? _xrm.AnnotationSet.Where(c => c.ObjectId.Id.Equals(objectId)).OrderBy(c => c.ModifiedOn).ToList() : _xrm.AnnotationSet.Where(c => c.ObjectId.Id.Equals(objectId) && c.AnnotationId.Value.Equals(noteId)).ToList();

                return ListNotes(notes);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
        public List<NotesInfo> ListNotes(List<Annotation> notes)
        {
            var list = new List<NotesInfo>();
            foreach (Annotation note in notes)
            {
                list.Add(new NotesInfo
                {
                    NoteId = note.AnnotationId.Value.ToString(),
                    Description = note.NoteText,
                    FileName = note.FileName,
                    NoteText = note.Subject != null ? note.Subject : string.Empty,
                    ModifiedOn = note.ModifiedOn.Value.ToLocalTime().ToString(CultureInfo.InvariantCulture),
                    DocumentBody = note.DocumentBody
                });
            }
            return list;
        }
        public class NotesInfo
        {
            public string NoteId { get; set; }
            public string NoteText { get; set; }
            public string FileName { get; set; }
            public string Description { get; set; }
            public string ModifiedOn { get; set; }

            public string DocumentBody { get; set; }
        }


        #endregion

        #region XRM Article

        //Retrieve articles by subject
        public List<KbArticle> RetrieveArticlesBySubjectId(Guid subjectId)
        {
            try
            {
                var articles = (subjectId == Guid.Empty) ? _xrm.KbArticleSet.Where(c => c.StatusCode.Value.Equals(3)).OrderBy(c => c.ModifiedOn).ToList() :
                                               _xrm.KbArticleSet.Where(c => (c.StatusCode.Value.Equals(3) && c.SubjectId.Id.Equals(subjectId))).OrderBy(c => c.ModifiedOn).ToList();

                return articles;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public List<ArticleItem> ListArticles(List<KbArticle> articles)
        {
            var list = new List<ArticleItem>();
            foreach (var article in articles)
            {
                list.Add(new ArticleItem
                {
                    ArticleNumber = article.Number,
                    Title = article.Title,
                    Subject = article.SubjectId.Name,
                    LastUpdate = article.ModifiedOn.Value.ToLocalTime().ToString(CultureInfo.InvariantCulture)
                });
            }
            return list;
        }

        public class ArticleItem
        {
            public string ArticleNumber { get; set; }
            public string Title { get; set; }
            public string Subject { get; set; }
            public string LastUpdate { get; set; }
        }

        #endregion

        #region XRM Subject

        //Retrieve subjects by parent
        public List<Subject> RetrieveSubjectByParent(string parentSubject)
        {
            try
            {
                var subjects = _xrm.SubjectSet.ToList().Where(c => (c.ParentSubject != null && c.ParentSubject.Name.Equals(parentSubject))).OrderBy(c => c.Title).ToList();
                return subjects;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        #endregion

        #region XRM Service Calendar

        //Retrieve appointments by party
        public List<ServiceAppointment> RetrieveAppointmentByParty(Guid partyId)
        {
            try
            {
                List<ServiceAppointment> serviceAppointments = new List<ServiceAppointment>();

                var appointments = _xrm.ServiceAppointmentSet.Where(c => c.ActivityTypeCode.Equals("serviceappointment")).ToList();

                if (appointments != null)
                {
                    foreach (ServiceAppointment appointment in appointments)
                    {
                        foreach (ActivityParty party in appointment.Customers)
                        {
                            if (party.PartyId.Id == partyId)
                            {
                                serviceAppointments.Add(appointment);
                            }

                        }
                    }

                    return serviceAppointments;
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            return null;
        }

        //Retrieve appointment occupied date by resource
        public List<String> RetrieveAppointmentOccupiedDateByResource(Guid partyId)
        {
            try
            {
                List<String> occupiedDate = new List<String>();

                var appointments = _xrm.ServiceAppointmentSet.Where(c => c.ActivityTypeCode.Equals("serviceappointment")).ToList();

                if (appointments != null)
                {
                    foreach (ServiceAppointment appointment in appointments)
                    {
                        foreach (ActivityParty party in appointment.Resources)
                        {
                            if (party.PartyId.Id == partyId)
                            {
                                occupiedDate.Add(appointment.ScheduledStart.Value.Date.ToShortDateString());
                            }

                        }
                    }

                    return occupiedDate;
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            return null;
        }

        #endregion

        #region XRM Resource

        //Retrieve resource by site
        public List<Resource> RetrieveResourceByObjectType(string objectType)
        {
            try
            {
                var resources = (objectType == null ? _xrm.ResourceSet.ToList<Resource>().ToList() :
                                 _xrm.ResourceSet.ToList<Resource>().Where(c => c.ObjectTypeCode.Equals(objectType) && (c.IsDisabled == false)).OrderBy(c => c.Name).ToList());

                if (resources != null)
                {
                    return resources;
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            return null;
        }

        #endregion

        #region XRM Customer - Contact

        //Retrieve sub-contacts by contact
        public List<Contact> RetrieveCustomers(Guid parentcustomerId, Guid customerId, bool inclMe)
        {
            try
            {
                List<Contact> contacts = null;

                if (inclMe)
                {
                    contacts = customerId == Guid.Empty ? _xrm.ContactSet.Where(c => (c.ParentCustomerId.Id.Equals(parentcustomerId) || c.ContactId.Value.Equals(parentcustomerId))).OrderByDescending(c => c.ModifiedOn).ToList() : _xrm.ContactSet.Where(c => ((c.ParentCustomerId.Id.Equals(parentcustomerId) || c.ContactId.Value.Equals(parentcustomerId)) && c.ContactId.Equals(customerId))).ToList();
                }
                else
                {
                    contacts = customerId == Guid.Empty ? _xrm.ContactSet.Where(c => c.ParentCustomerId.Id.Equals(parentcustomerId)).OrderByDescending(c => c.ModifiedOn).ToList() : _xrm.ContactSet.Where(c => ((c.ParentCustomerId.Id.Equals(parentcustomerId) && c.ContactId.Equals(customerId)))).ToList();
                }

                return contacts;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public List<ContactItem> ListCustomers(List<Contact> contacts)
        {
            var list = new List<ContactItem>();

            foreach (Contact contact in contacts)
            {
                list.Add(new ContactItem
                {
                    Id = contact.ContactId.Value.ToString(),
                    FullName = contact.FullName,
                    City = contact.Address1_City,
                    Telephone = contact.Address1_Telephone1,
                    Email = contact.EMailAddress1,
                    LastUpdate = contact.ModifiedOn.Value.ToLocalTime().ToString(CultureInfo.InvariantCulture)
                });
            }
            return list;
        }

        public class ContactItem
        {
            public string Id { get; set; }
            public string FullName { get; set; }
            public string City { get; set; }
            public string Telephone { get; set; }
            public string Email { get; set; }
            public string LastUpdate { get; set; }
        }

        #endregion

        #region XRM Order

        //Retrieve customer orders.
        public List<OrderItem> RetrieveCustomerOrders(Guid parentcustomerId, string orderNumber)
        {
            try
            {
                var idList = new List<string> { parentcustomerId.ToString() };

                var contacts = _xrm.ContactSet.Where(c => c.ParentCustomerId != null && c.ParentCustomerId.Id != null && c.ParentCustomerId.Id.Equals(parentcustomerId));
                foreach (Contact contact in contacts)
                {
                    if (contact.ContactId != null)
                    {
                        idList.Add(contact.ContactId.Value.ToString());
                    }
                }

                var orders = ((orderNumber == null) ? _xrm.SalesOrderSet.Where(o => o.CustomerId != null).OrderByDescending(o => o.ModifiedOn) :
                                               _xrm.SalesOrderSet.Where(o => o.OrderNumber.Equals(orderNumber))).ToList();
                if (orders.Any())
                {
                    var customerOrders = new List<SalesOrder>();
                    foreach (SalesOrder order in orders)
                    {
                        if (idList.Contains(order.CustomerId.Id.ToString()))
                        {
                            customerOrders.Add(order);
                        }
                    }

                    return ListOrders(customerOrders);
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
            return null;
        }

        public List<OrderItem> ListOrders(List<SalesOrder> orders)
        {
            var list = new List<OrderItem>();
            foreach (SalesOrder order in orders)
            {
                if (order.SalesOrderId != null)
                {
                    list.Add(new OrderItem
                    {
                        Id = order.SalesOrderId.Value.ToString(),
                        OrderNumber = order.OrderNumber,
                        OrderStatus = GetMetaLabel("salesorder", "statuscode", order.StatusCode.Value, true),
                        CustomerName = order.CustomerId.Name,
                        CustomerId = order.CustomerId.Id.ToString(),
                        Description = order.Description != null ? order.Description : string.Empty,
                        LastUpdate = order.ModifiedOn.Value.ToLocalTime().ToString(CultureInfo.InvariantCulture)
                    });
                }
            }
            return list;
        }

        public class OrderItem
        {
            public string Id { get; set; }
            public string OrderNumber { get; set; }
            public string OrderStatus { get; set; }
            public string CustomerName { get; set; }
            public string CustomerId { get; set; }
            public string Description { get; set; }
            public string LastUpdate { get; set; }
        }

        #endregion

        #region General methods

        //Create a CRM record.
        public void CreateCrmRecord(Entity entity)
        {
            try
            {
                _xrm.AddObject(entity);
                _xrm.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        //Update the CRM record.
        public void UpdateCrmRecord(Entity entity)
        {
            try
            {
                var record = RetrieveCrmRecord(entity.LogicalName, entity.LogicalName + "id", entity.Id.ToString());

                foreach (var attribute in entity.Attributes)
                {
                    if (attribute.Value != null)
                    {
                        record[attribute.Key] = entity[attribute.Key];
                    }
                }

                _xrm.UpdateObject(record);
                _xrm.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        //Retrieve a CRM record.
        public Entity RetrieveCrmRecord(string entityName, string attributeName, string attributeValue)
        {
            try
            {
                var record = (from r in _xrm.CreateQuery(entityName)
                              where (string)r[attributeName] == attributeValue
                              select r);

                return record.SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        //Retrieve CRM records.
        public List<Entity> RetrieveCrmRecords(string entityName, string attributeName, string attributeValue)
        {
            try
            {
                var record = (from r in _xrm.CreateQuery(entityName)
                              where (string)r[attributeName] == attributeValue
                              select r);
                return record.ToList();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        #endregion

        #region Metadata Helper

        private string GetMetaLabel(string entityName, string attributeName, int attributeValue, bool isStatus)
        {
            var label = String.Empty;

            try
            {
                var attributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName,
                    RetrieveAsIfPublished = true
                };

                var attributeResponse = (RetrieveAttributeResponse)_xrm.Execute(attributeRequest);
                var attrMetadata = attributeResponse.AttributeMetadata;

                if (isStatus)
                {
                    //Query status label
                    var statusAttrMetadata = (StatusAttributeMetadata)attrMetadata;
                    foreach (StatusOptionMetadata statusMeta in statusAttrMetadata.OptionSet.Options)
                    {
                        if (statusMeta.Value != attributeValue)
                        {
                            continue;
                        }
                        label = statusMeta.Label.UserLocalizedLabel.Label;
                        break;
                    }
                }
                else //picklist
                {
                    //Query picklist label
                    var picklistMetadata = (PicklistAttributeMetadata)attrMetadata;
                    foreach (OptionMetadata optionMeta in picklistMetadata.OptionSet.Options)
                    {
                        if (optionMeta.Value != attributeValue)
                        {
                            continue;
                        }
                        label = optionMeta.Label.UserLocalizedLabel.Label;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            return label;
        }

        #endregion

        #region Dispose class

        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool IsDisposed()
        {
            return _disposed;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _xrm.Dispose();
                }
            }
            _disposed = true;
        }

        #endregion
    }
}
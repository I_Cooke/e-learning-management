﻿using DynamicsCRMWebPortal.Models;

namespace DynamicsCRMWebPortal.ViewModels
{
    public class DynamicsCRMWebPortalViewModel
    {
        public DynamicsCRMWebPortalSettingsPart Settings { get; set; }
    }
}
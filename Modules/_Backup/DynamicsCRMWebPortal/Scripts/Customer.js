﻿ 
varURL = varURL + 'Customer';
var varUpdateURL = varURL + "/Update";
function UpdateCustomer(url) {
  
    var isValid = performValidation();
    if (isValid) {
        ajaxCall(varURL + '/Update/');
    }
    else {
        $("#errorMsg").show({ duration: 0, queue: true }).delay(3000).hide({ duration: 0, queue: true });
    }
}
function ajaxCall(actionUrl) {
    $.ajax({
        url: actionUrl,
        type: 'POST',
        data: getDataFromControls(),
        success: function (json) {
            $("#grid").trigger('reloadGrid');
            $('#spanMsg').html(json);

        },
        error: function (exError, xhr, msg) {
            alert(msg);
        }

    });
    $('#editCustomerDlg').dialog('close');
    return false;
}
function getDataFromControls() {
    var model = new Object();
    model.FirstName = $('#FirstName').val();
    model.LastName = $('#LastName').val();
    model.AddressName = $('#AddressName').val();
    model.AddressLine1 = $('#AddressLine1').val();
    model.AddressLine2 = $('#AddressLine2').val();
    model.AddressLine3 = $('#AddressLine3').val();
    model.City = $('#City').val();
    model.County = $('#County').val();
    model.Country = $('#Country').val();
    model.EmailAddress = $('#EmailAddress').val();
    model.Telephone = $('#Telephone').val();
    model.PostCode = $('#PostCode').val();
    model.CustomerId = $('#CustomerId').val();
    model.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();

    var varData = JSON.parse(JSON.stringify(model))
    return varData;
}
function openCustomer() {
    $('#spanMsg').html('');
    var varRowData = {};
    varRowData.Id = '';
    varRowData.FirstName = '';
    varRowData.LastName = '';
    varRowData.Address1_Name = '';
    varRowData.Address1_Line1 = '';
    varRowData.Address1_Line2 = '';
    varRowData.Address1_Line3 = ''; 
    varRowData.Address1_County = '';
    varRowData.Address1_Country = '';
    varRowData.EMailAddress1 = '';
    varRowData.Address1_Telephone1 = '';
    varRowData.Address1_PostalCode = '';
    varRowData.Address1_City = '';
    fillDetail(varRowData);
    $('#btnCreateCustomer').show();
    $('#btnUpdateCustomer').hide();
    $('#editCustomerDlg').attr("title", "Create Customer");
    $('#editCustomerDlg').dialog('open');
    $('.ui-dialog-title').html('New Customer');
    return false;
}
function fillDetail(varRowData) {
    $('#FirstName').val(varRowData.FirstName);
    $('#LastName').val(varRowData.LastName);
    $('#AddressName').val(varRowData.Address1_Name);
    $('#AddressLine1').val(varRowData.Address1_Line1);
    $('#AddressLine2').val(varRowData.Address1_Line2);
    $('#AddressLine3').val(varRowData.Address1_Line3);
    $('#County').val(varRowData.Address1_County);
    $('#Country').val(varRowData.Address1_Country);
    $('#City').val(varRowData.Address1_City);
    $('#EmailAddress').val(varRowData.EMailAddress1);
    $('#Telephone').val(varRowData.Address1_Telephone1);
    $('#PostCode').val(varRowData.Address1_PostalCode);
    $('#CustomerId').val(varRowData.Id);
}
function ClosePopup() {
    $('#editCustomerDlg').dialog('close');
}
function CreateCustomer() {
    var isValid = performValidation();
    if (isValid)
    {
        ajaxCall(varURL + '/Create/');
    }
    else {
        $("#errorMsg").show({ duration: 0, queue: true }).delay(3000).hide({ duration: 0, queue: true });
    }
}
function performValidation() {
    $("#errorMsg").empty();
    var chckFirstName = chckValidOREmpty('FirstName');
    var chckLastName = chckValidOREmpty('LastName');
    var chckEmail = chckValidOREmpty('EmailAddress');
    var chckPhnNum = chckValidOREmpty('Telephone');
    var ValidPhnNum = chckValidPhnNum(chckPhnNum);
    var isValid = chckFirstName && chckLastName && chckEmail && ValidPhnNum;
    return isValid;
}
function chckValidOREmpty(control) {
    if (($('#' + control).val() == '') || ($('#' + control).val() == null) || ($('#' + control).val() == '-1')) {
        $('#errorMsg').append('<span style="color:red">Please enter' + ' ' + $('label[for=' + control + ']').text() + '<span>');
        $('#errorMsg').append('<br/>');
        $('#control').focus();
        return false;
    }
    else {
        return true;
    }
}
function chckValidPhnNum(chckPhnNum) {
    if (chckPhnNum) {
        var pattern = /^[0-9]+$/;
        if (pattern.test($('#Telephone').val())) {
            return true;

        }
        else {
            $('#errorMsg').append('<span style="color:red">Invalid Phone Number<span>');
            $('#errorMsg').append('<br/>');
            return false;
        }


    }
}
function EditPopup(rowId) {
    $('#spanMsg').html('');
    //alert(url);
    var varRowData = jQuery('#grid').jqGrid('getRowData', rowId);
    fillDetail(varRowData);
    $('#btnCreateCustomer').hide();
    $('#btnUpdateCustomer').show();
    $('.ui-dialog-title').html('Edit Customer');
    $('#editCustomerDlg').dialog('open');
    $('#editCustomerDlg').attr("title", "Edit Customer");

    return false;
}
//*****ready***//
$(function () {
    $('#editCustomerDlg').dialog({
        modal: true,
        width: 700,
        resizable: false,
        open: function () {
            $('#editCustomerDlg').css('display', 'block');
        },
        close: function () {
            $('#editCustomerDlg').css('display', 'none');
        }
    });
    $('#editCustomerDlg').dialog('close');
    function editLink(cellValue, options, rowdata, action) {
        return '<a onclick="EditPopup(\'' + rowdata.Id + '\')" class="ui-icon ui-icon-pencil" ></a>';
    }

    $("#grid").jqGrid({
        url: varURL + "/GetCustomers",
        datatype: 'json',
        type: 'Get',
        colNames: ['Id', 'Customer', 'First Name', 'Last Name', 'Address','Address Line1', 'Address Line2', 'Address Line3', 'County',
                        'Country', 'Email Address', 'City', 'Postal Code', 'Telephone', 'Edit'],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
            { key: false, hidden: true, name: 'FullName', index: 'FullName', editable: true },
            { key: false, hidden: false, name: 'FirstName', index: 'FirstName', editable: true },
            { key: false, hidden: false, name: 'LastName', index: 'LastName', editable: true },
            { key: false, hidden: false, name: 'Address1_Name', index: 'Address1_Name', editable: true },
            { key: false, hidden: false, name: 'Address1_Line1', index: 'Address1_Line1', editable: true },
            { key: false, hidden: false, name: 'Address1_Line2', index: 'Address1_Line2', editable: true },
            { key: false, hidden: false, name: 'Address1_Line3', index: 'Address1_Line3', editable: true },
            { key: false, hidden: false, name: 'Address1_County', index: 'Address1_County', editable: true },
            { key: false, hidden: false, name: 'Address1_Country', index: 'Address1_Country', editable: true },
            { key: false, hidden: false, name: 'EMailAddress1', index: 'EMailAddress1', editable: true },
            { key: false, hidden: false, name: 'Address1_City', index: 'Address1_City', editable: true },
            { key: false, hidden: false, name: 'Address1_PostalCode', index: 'Address1_PostalCode', editable: true },
            { key: false, hidden: false, name: 'Address1_Telephone1', index: 'Address1_Telephone1', editable: true },
            { key: false, name: 'Edit', search: false, index: 'Id', width: 30, sortable: false, formatter: editLink }
        ],
        pager: jQuery('#pager'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: 'Customer List',
        emptyrecords: 'No records to display',
        formatter: 'actions',
        formatoptions: {
            keys: false,
            editbutton: true,
            delbutton: false,
            editformbutton: false,
            onSuccess: function (response) {
                if (response.status == 200) {
                }
            },
            extraparam: { oper: 'edit' }
        },
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#pager', { edit: false, add: false, del: false, search: false, refresh: false });
    $('#first_pager').prop('title', 'First');
    $('#prev_pager').prop('title', 'Previous');
    $('#last_pager').prop('title', 'Last');
    $('#next_pager').prop('title', 'Next');
});
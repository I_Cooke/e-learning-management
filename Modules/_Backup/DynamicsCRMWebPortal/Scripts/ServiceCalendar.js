﻿
varURL = varURL + 'Calendar';
function ClosePopup() {
    $('#divCalendar').dialog('close');
}
function ajaxCall(actionUrl) {
    $.ajax({
        url: actionUrl,
        type: 'POST',
        data: getDataFromControls(),
        success: function (json) {
            $('#spanMsg').html(json);
            GetCalendarData();

        },
        error: function (exError, xhr, msg) {
            alert(msg);
        }

    });
    $('#divCalendar').dialog('close');
    return false;
}
function getDataFromControls() {
    var model = new Object();
    model.ResourceItemId = $('#ResourceItemId').val();
    model.StartTime = $("input[name='StartTime']").val();
    model.Subject = $('#Subject').val();
    model.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();

    var varData = JSON.parse(JSON.stringify(model))
    return varData;
}
function openCalendar() {
    $('#spanMsg').html('');
    var varRowData = {};
    varRowData.ResourceItemId = '';
    varRowData.Subject = '';
    varRowData.StartTime = '';
    fillDetail(varRowData);
    $('#divCalendar').dialog('open');
    $('.ui-dialog-title').html('Add New Service');
    return false;
}
function CreateServiceCalendar() {
    var isValid = performValidation();
    if (isValid) {
        ajaxCall(varURL + '/Create/');
    }
    else {
        $("#errorMsg").show({ duration: 0, queue: true }).delay(3000).hide({ duration: 0, queue: true });
    }

}
function performValidation() {
    $("#errorMsg").empty();
    var chckReSourceTime = chckValidOREmpty('ResourceItemId');
    var chckSubject = chckValidOREmpty('Subject');
    var chckStartTime = chckValidOREmpty('StartTime');
    var isValid = chckSubject && chckStartTime && chckReSourceTime;
    return isValid;
}
function chckValidOREmpty(control) {
    if (($('#' + control).val() == '') || ($('#' + control).val() == null) || ($('#' + control).val() == '-1')) {
        $('#errorMsg').append('<span style="color:red">Please enter' + ' ' + $('label[for=' + control + ']').text() + '<span>');
        $('#errorMsg').append('<br/>');
        $('#control').focus();
        return false;
    }
    else {
        return true;
    }
}
function fillDetail(varRowData) {
    $('#ResourceItemId').val(varRowData.ResourceItemId);
    $('#Subject').val(varRowData.Subject);
    $('#StartTime').val(varRowData.StartTime);



}
function GetCalendarData() {
    $.ajax({
        url: varURL + '/GetCalendarData',
        success: function (json) {
            InitializeCalendar(json);
            //$('.fc-button-group,.fc button').css('display', 'block');

        },
        error: function (exError, xhr, msg) {
            // alert(msg);
        }

    });
    //  $('#editOpportunityDlg').dialog('close');
    return false;
}
$(document).ready(function () {
    $("#StartTime").datepicker({ minDate: 0 });
    $('#divCalendar').dialog({
        modal: true,
        width: 254,
        open: function () {
            $('#divCalendar').css('display', 'block');

        },
        resizable: false,
        close: function () {
            $('#divCalendar').css('display', 'none');
        }
    });
    $('#divCalendar').dialog('close');
    $('#external-events .fc-event').each(function () {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    });
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: true,
        events: [],
        eventLimit: true, // allow "more" link when too many events 
        loading: function (bool) {
            $('#loading').toggle(bool);
        },
        droppable: true

    });
    GetCalendarData();
    $('.fc-month-button,fc-button,fc-state-default,fc-corner-left,fc-state-active').html('Month');
    $('.fc-agendaWeek-button,fc-button,fc-state-default').html('Week');
    $('.fc-agendaDay-button,fc-button,fc-state-default,fc-corner-right').html('Day');
    $('.fc-today-button,fc-button,fc-state-default,fc-corner-left,fc-corner-right').html('Today');
    //$('.fc-button-group,.fc button').css('display', 'block');
});
function appendZero(n) {
    return n > 9 ? "" + n : "0" + n;
}
function InitializeCalendar(eventsData) {
    var vDate;
    for (var i = 0; i < eventsData.length; i++) {
        vDate = eventsData[i]["start"];
        if (vDate != null && vDate != undefined && vDate != "") {
            var varArrDate = vDate.split(' ');
            eventsData[i]["start"] = varArrDate[0] + '-' + appendZero(parseInt(varArrDate[1])) + '-' + appendZero(parseInt(varArrDate[2])) + 'T00:00:00';
            eventsData[i]["end"] = varArrDate[0] + '-' + appendZero(parseInt(varArrDate[1])) + '-' + appendZero(parseInt(varArrDate[2])) + 'T00:00:00';
        }
    } 
    $('#calendar').fullCalendar('removeEvents');
    $('#calendar').fullCalendar('addEventSource', eventsData);
    $('#calendar').fullCalendar('refetchEvents');


}
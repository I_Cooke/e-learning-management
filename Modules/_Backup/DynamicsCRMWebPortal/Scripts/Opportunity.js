﻿varURL = varURL + 'Opportunity';
var varUpdateURL = varURL + '/Update';
function UpdateOpportunityDetails(url) {
    var isValid = performValidation();
    if (isValid) {
        ajaxCall(varUpdateURL);
    }

    else {
        $("#errorMsg").show({ duration: 0, queue: true }).delay(2000).hide({ duration: 0, queue: true })
    }

}
function ajaxCall(actionUrl) {
    $.ajax({
        url: actionUrl,
        type: 'POST',
        data: getDataFromControls(),
        success: function (json) {
            $("#grid").trigger('reloadGrid');
            $('#spanMsg').html(json);

        },
        error: function (exError, xhr, msg) {
            alert(msg);
        }

    });
    $('#editOpportunityDlg').dialog('close');
    return false;
}

function getDataFromControls() {
    var model = new Object();
    model.Name = $('#Topic').val();
    model.BudgetAmount = Math.round($('#BudgetAmount').val() * 100) / 100;
    model.Description = $('#Description').val();
    model.PurchaseTimeframe = $('#PurchaseTimeframe').val();
    model.PurchaseProcess = $('#PurchaseProcess').val();
    model.OpportunityId = $('#OpportunityId').val();
    model.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();

    var varData = JSON.parse(JSON.stringify(model))
    return varData;
}
function ClosePopup() {
    $('#editOpportunityDlg').dialog('close');
}
function openOpportunity() {
    $('#spanMsg').html('');
    var varRowData = {};
    varRowData.Name = '';
    varRowData.Description = '';
    varRowData.BudgetAmount = '';
    varRowData.PurchaseTimeFrame = '-1';
    varRowData.PurchaseProcess = '-1';
    fillDetail(varRowData);
    $('#btnCreateOpportunity').show();
    $('#btnUpdateOpportunity').hide();
    $('#editOpportunityDlg').attr("title", "Create Opportunity");
    $('#editOpportunityDlg').dialog('open');
    $('.ui-dialog-title').html('New Opportunity');
    return false;
}
function CreateOpportunity() {

    var isValid = performValidation();

    if (isValid) {
        ajaxCall(varURL + '/Create/');
    }
    else {
        $("#errorMsg").show({ duration: 0, queue: true }).delay(3000).hide({ duration: 0, queue: true });
    }

}
function performValidation() {
    $("#errorMsg").empty();
    var chckTopic = chckValidOREmpty('Topic');
    var chckBudgetAmt = chckValidOREmpty('BudgetAmount');
    var validBudget = chckValidBudgetAmt(chckBudgetAmt);
    var chckDescription = chckValidOREmpty('Description');
    var chckPurchaseTime = chckValidOREmpty('PurchaseTimeframe');
    var chckPurchaseProcess = chckValidOREmpty('PurchaseProcess');
    var isValid = chckTopic && chckBudgetAmt && chckDescription && chckPurchaseTime && chckPurchaseProcess && validBudget;
    return isValid;
}
function chckValidOREmpty(control) {
    if (($('#' + control).val() == '') || ($('#' + control).val() == null) || ($('#' + control).val() == '-1')) {
        $('#errorMsg').append('<span style="color:red">Please enter' + ' ' + $('label[for=' + control + ']').text() + '<span>');
        $('#errorMsg').append('<br/>');
        $('#control').focus();

        return false;
    }
    else {
        return true;
    }
}
function chckValidBudgetAmt(chckBudgetAmt) {
    if (chckBudgetAmt) {
        var pattern = /^[-]?([1-9]{1}[0-9]{0,}(\.[0-9]{0,9})?|0(\.[0-9]{0,9})?|\.[0-9]{1,9})$/;
        if (pattern.test($('#BudgetAmount').val())) {
            return true;

        }
        else {
            $('#errorMsg').append('<span style="color:red">Invalid Budget Amount<span>');
            $('#errorMsg').append('<br/>');
            return false;
        }
    }
}
function fillDetail(varRowData) {
    $('#Topic').val(varRowData.Name);
    $('#BudgetAmount').val(varRowData.BudgetAmount);
    $('#Description').val(varRowData.Description);
    $('#PurchaseTimeframe').val(varRowData.PurchaseTimeFrame);
    $('#PurchaseProcess').val(varRowData.PurchaseProcess);
    $('#OpportunityId').val(varRowData.Id);

}
function EditPopup(rowId) {
    $('#spanMsg').html('');
    //alert(url);
    var varRowData = jQuery('#grid').jqGrid('getRowData', rowId);
    fillDetail(varRowData);
    $('#btnCreateOpportunity').hide();
    $('#btnUpdateOpportunity').show();
    $('#editOpportunityDlg').attr("title", "Edit Opportunity");
    $('.ui-dialog-title').html('Edit Opportunity');

    $('#editOpportunityDlg').dialog('open');
    //href = url;
    //$('#editOpportunityDlg').dialog({
    //    modal: true,
    //    width: 500,
    //    open: function (event, ui) {
    //        $(this).load(href, function (result) {
    //            //$('#frmOpportunity').submit(function () {
    //            //    $.ajax({
    //            //        url: this.action,
    //            //        type: this.method,
    //            //        data: $(this).serialize(),
    //            //        success: function (json) {
    //            //            $('#editOpportunityDlg').dialog('close');
    //            //            //$('#NewValue').val(json.newValue);
    //            //        }
    //            //    });
    //            //    return false;
    //            //});
    //        });
    //    }
    //});
    return false;
}
$(function () {
    $('#editOpportunityDlg').dialog({
        modal: true,
        width: 560,
        resizable: false,
        open: function () {
            $('#editOpportunityDlg').css('display', 'block');
        },
        close: function () {
            $('#editOpportunityDlg').css('display', 'none');
        }
    });
    $('#editOpportunityDlg').dialog('close');
    function editLink(cellValue, options, rowdata, action) {
        //return '<a href="' + varUpdateURL + '?opportunityId=' + rowdata.Id + '" class="ui-icon ui-icon-pencil" ></a>';
        return '<a onclick="EditPopup(\'' + rowdata.Id + '\')" class="ui-icon ui-icon-pencil" ></a>';
    }

    $("#grid").jqGrid({
        url: varURL + '/GetOpportunities',
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Id', 'Topic', 'Purchase Timeframe', 'Budget Amount($) ', 'Purchase Process', 'Description', 'Edit'],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
            { key: false, name: 'Name', index: 'Name', editable: true },
            {
                key: false, name: 'PurchaseTimeFrame', index: 'PurchaseTimeFrame', editable: true, formatter: 'select', edittype: 'select',
                editoptions: {
                    value: '0:Immediate;1:This Quarter;2:Next Quarter;3:This Year;4:UnKnown:'
                }
            },
            { key: false, name: 'BudgetAmount', index: 'BudgetAmount', editable: true, align: 'right' },
            {
                key: false, name: 'PurchaseProcess', index: 'PurchaseProcess', editable: true, edittype: 'select', formatter: 'select',
                editoptions: { value: '0:Individual;1:Commitee;2:Unknown' }
            },
            { key: false, name: 'Description', index: 'Description', editable: true },
            { key: false, name: 'Edit', search: false, index: 'Id', width: 30, sortable: false, formatter: editLink }
        ],
        pager: jQuery('#pager'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: 'Opportunity List',
        emptyrecords: 'No records to display',
        formatter: 'actions',
        formatoptions: {
            keys: false,
            editbutton: true,
            delbutton: false,
            editformbutton: false,
            onSuccess: function (response) {
                if (response.status == 200) {
                }
            },
            extraparam: { oper: 'edit' }
        },
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#pager', { edit: false, add: false, del: false, search: false, refresh: false },
        {
            // edit  
            zIndex: 100,
            url: '/DynamicsCRMPortal/Opportunity/UpdateOpportunity',
            closeOnEscape: true,
            closeAfterEdit: true,
            recreateForm: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            // add  
            zIndex: 100,
            url: "/DynamicsCRMPortal/Opportunity/Create",
            closeOnEscape: true,
            closeAfterAdd: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            // delete  
            zIndex: 100,
            url: "/DynamicsCRMPortal/Opportunity/Delete",
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            msg: "Are you sure you want to delete this opportunity?",
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        });
    $('#first_pager').prop('title', 'First');
    $('#prev_pager').prop('title', 'Previous');
    $('#last_pager').prop('title', 'Last');
    $('#next_pager').prop('title', 'Next');
});
﻿ 
varURL = varURL + 'Order'; 
var varUpdateURL = varURL + '/Update';
function UpdateOrder(url) {
    
   ajaxCall(varUpdateURL);
}
function ajaxCall(actionUrl) {
    $.ajax({
        url: actionUrl,
        type: 'POST',
        data: getDataFromControls(),
        success: function (json) {
            $("#grid").trigger('reloadGrid');
            $('#spanMsg').html(json);

        },
        error: function (exError, xhr, msg) {
            alert(msg);
        }

    });
    $('#editOrderDlg').dialog('close');
    return false;
}
function getDataFromControls() {
    var model = new Object();
    model.CustomerId = $('#CustomerId').val();
    model.Description = $('#Description').val();
    model.OrderId = $('#OrderId').val();
    model.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();

    var varData = JSON.parse(JSON.stringify(model))
    return varData;
}
function openOrder() {
    $('#spanMsg').html('');
    var varRowData = {};
    varRowData.Id = '';
    varRowData.OrderNumber = '';
    varRowData.CustomerName = '';
    varRowData.Description = '';
    fillDetail(varRowData);
    $('#btnCreateOrder').show();
    $('#btnUpdateOrder').hide();
    $('#editOrderDlg').attr("title", "Create Order");
    $('#editOrderDlg').dialog('open');
    $('.ui-dialog-title').html('New Order');
    return false;
}
function fillDetail(varRowData) {

}
function ClosePopup() {
    $('#editOrderDlg').dialog('close');
}
function CreateOrder() {
    var isValid = performValidation();
    if (isValid) {
        ajaxCall(varURL + '/Create/');
    }
    else {
        $("#errorMsg").show({ duration: 0, queue: true }).delay(3000).hide({ duration: 0, queue: true });
    }
    
}
function performValidation() {
    $("#errorMsg").empty();
    var chckDescription = chckValidOREmpty('Description');
    var isValid = chckDescription;
    return isValid;
}
function chckValidOREmpty(control) {
    if (($('#' + control).val() == '') || ($('#' + control).val() == null) || ($('#' + control).val() == '-1')) {
        $('#errorMsg').append('<span style="color:red">Please enter' + ' ' + $('label[for=' + control + ']').text() + '<span>');
        $('#errorMsg').append('<br/>');
        $('#control').focus();
        return false;
    }
    else {
        return true;
    }
}
function EditPopup(rowId) {
    $('#spanMsg').html('');
    //alert(url);
    var varRowData = jQuery('#grid').jqGrid('getRowData', rowId);
    fillDetail(varRowData);
    $('#btnCreateOrder').hide();
    $('#btnUpdateOrder').show();
    $('.ui-dialog-title').html('Edit Order');
    $('#editOrderDlg').dialog('open');
    $('.ui-dialog-title').html('Edit Order');
    $('#editOrderDlg').attr("title", "Edit Order");

    return false;
}
//*****ready***//
$(function () {
    $('#editOrderDlg').dialog({
        modal: true,
        width: 380,
        open: function () {
            $('#editOrderDlg').css('display', 'block');
        },
        close: function () {
            $('#editOrderDlg').css('display', 'none');
        }
    });
    $('#editOrderDlg').dialog('close');
    function editLink(cellValue, options, rowdata, action) {
        return '<a onclick="EditPopup(\'' + rowdata.Id + '\')" class="ui-icon ui-icon-pencil" ></a>';
    }

    $("#grid").jqGrid({
        url:   varURL + '/GetOrders',
        datatype: 'json',
        type: 'Get',
        colNames: ['Id', 'OrderNumber', 'Customer', 'Description', 'OrderStatus' ],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
            { key: false, hidden: false, name: 'OrderNumber', index: 'OrderNumber', editable: true },
            { key: false, hidden: false, name: 'CustomerName', index: 'CustomerName', editable: true },
            { key: false, hidden: false, name: 'Description', index: 'Description', editable: true },
            { key: false, hidden: false, name: 'OrderStatus', index: 'OrderStatus', editable: true }
            //,
            //{ key: false, name: 'Edit', search: false, index: 'Id', width: 30, sortable: false, formatter: editLink }
        ],
        pager: jQuery('#pager'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: 'Order List',
        emptyrecords: 'No records to display',
        formatter: 'actions',
        formatoptions: {
            keys: false,
            editbutton: true,
            delbutton: false,
            editformbutton: false,
            onSuccess: function (response) {
                if (response.status == 200) {
                }
            },
            extraparam: { oper: 'edit' }
        },
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#pager', { edit: false, add: false, del: false, search: false, refresh: false });
    $('#first_pager').prop('title', 'First');
    $('#prev_pager').prop('title', 'Previous');
    $('#last_pager').prop('title', 'Last');
    $('#next_pager').prop('title', 'Next');
});
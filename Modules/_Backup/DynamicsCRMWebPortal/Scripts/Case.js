﻿varURL = varURL + 'Case';
var varUpdateURL = varURL + "/Update";
function UpdateCase(url) {
    var isValid = performValidation();
    if (isValid) {
        ajaxCall(varUpdateURL);
    }

    else {
        $("#errorMsg").show({ duration: 0, queue: true }).delay(2000).hide({ duration: 0, queue: true })
    }
}
function ajaxCall(actionUrl) {
    $.ajax({
        url: actionUrl,
        type: 'POST',
        data: getDataFromControls(),
        success: function (json) {
            $("#grid").trigger('reloadGrid');
            $('#spanMsg').html(json);

        },
        error: function (exError, xhr, msg) {
            alert(msg);
        }

    });
    $('#editCaseDlg').dialog('close');
    return false;
}
function getDataFromControls() {
    var model = new Object();
    model.Title = $('#Title').val();
    model.Description = $('#Description').val();
    model.CaseType = $('#CaseType').val();
    model.IncidentId = $("#IncidentId").val();
    model.TicketNumber = $("#TicketNumber").val();
    if ($("#Notes").val() != '')
    {
        model.Note = $("#Notes").val();
    }
   
    //model.NoteAttachment = $('#NotesFile').val().split('\\').pop();
   
    model.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();

    var varData = JSON.parse(JSON.stringify(model));
  
    return varData;
}
function ClosePopup() {
    $('#editCaseDlg').dialog('close');
}
function openCase() {
    $('#spanMsg').html('');
    var varRowData = {};
    varRowData.Id = '';
    varRowData.Title = '';
    varRowData.Status = 0;
    varRowData.Description = '';
    varRowData.CaseTypeValue = '-1';
    fillDetail(varRowData);
    $('#btnCreateCase').show();
    $('#btnUpdateCase').hide();
    $('#editCaseDlg').attr("title", "Create Case");
    $('#editCaseDlg').dialog('open');
    $("tr:nth-child(4),tr:nth-child(5),tr:nth-child(6)").hide();
    $('.ui-dialog-title').html('New Case');
    return false;
}
function CreateCase() {
    var isValid = performValidation();

    if (isValid) {
        ajaxCall(varURL + '/Create/');
    }
    else {
        $("#errorMsg").show({ duration: 0, queue: true }).delay(3000).hide({ duration: 0, queue: true });
    }
}
function performValidation() {
    $("#errorMsg").empty();
    var chckTitle = chckValidOREmpty('Title');
    var chckCaseType = chckValidOREmpty('CaseType');
    var chckDescription = chckValidOREmpty('Description');
    var isValid = chckTitle && chckCaseType && chckDescription;
    return isValid;
}
function chckValidOREmpty(control) {
    if (($('#' + control).val() == '') || ($('#' + control).val() == null) || ($('#' + control).val() == '-1')) {
        $('#errorMsg').append('<span style="color:red">Please enter' + ' ' + $('label[for=' + control + ']').text() + '<span>');
        $('#errorMsg').append('<br/>');
        $('#control').focus();
        return false;
    }
    else {
        return true;
    }
}
function fillDetail(rowData, noteData) {
    $('#Title').val(rowData.Title);
    $('#Description').val(rowData.Description);
    $('#IncidentId').val(rowData.Id);
    $('#TicketNumber').val(rowData.TicketNumber);
    $('#CaseType').val(rowData.CaseTypeValue);
    $("#LastModified").empty();
    $("#NoteAttachment").empty();
    if (noteData != null) {
        if (noteData.NotesData.length != 0) {
            if (noteData.NotesData[0].FileName != null)
            {
                $("#lstModified").show();
                $("#lstAttachment").show();
                $("#LastModified").append(noteData.NotesData[0].ModifiedOn);
                $("#NoteAttachment").append('<a target=_blank href=' + varURL + '/NoteAttachment?incidentId=' + rowData.Id + '&noteId=' + noteData.NotesData[0].NoteId + '>' + noteData.NotesData[0].FileName + '</a>');
            }
          
        }
    }
}
function EditPopup(Id) {
    $('#spanMsg').html('');

    var rowData = jQuery('#grid').jqGrid('getRowData', Id)
    var actionUrl = varUpdateURL;
    var TicketNumber = rowData.TicketNumber;
    $.get(actionUrl, { ticketNumber: TicketNumber }, function (NoteData) {
        if (NoteData != null) {
            fillDetail(rowData, NoteData);
        }
    })
     .fail(function () {
         $('#spanMsg').html('Error');
     });

    $('#btnCreateCase').hide();
    $('#btnUpdateCase').show();
    $('.ui-dialog-title').html('Edit Case');
    $("tr:nth-child(4),tr:nth-child(5),tr:nth-child(6)").show();
    $("#lstModified").hide();
    $("#lstAttachment").hide();
    $("#Notes").val('');
    $('#editCaseDlg').dialog('open');
    $('.ui-dialog-title').html('Edit Case');
    $('#editCaseDlg').attr("title", "Edit Case");
    return false;
}
//*****ready***//
$(function () {
    $('#editCaseDlg').dialog({
        modal: true,
        width: 315,
        resizable: false,
        open: function () {
            $('#editCaseDlg').css('display', 'block');
        },
        close: function () {
            $('#editCaseDlg').css('display', 'none');
            $("tr:nth-child(4),tr:nth-child(5),tr:nth-child(6)").show();
        }
    });
    $('#editCaseDlg').dialog('close');
    function editLink(cellValue, options, rowdata, action) {

        return '<a onclick="EditPopup(\'' + rowdata.Id + '\')" class="ui-icon ui-icon-pencil" ></a>';
    }

    $("#grid").jqGrid({
        url: varURL + "/GetCases",
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Id', 'TicketNumber', 'Title', 'Description', 'Case Type', 'Case Status', 'Edit'],
        colModel: [
		    { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
            { key: true, hidden: true, name: 'TicketNumber', index: 'TicketNumber', editable: true },
            { key: false, name: 'Title', index: 'Title', editable: true },
            {
                key: false, name: 'Description', index: 'Description', editable: true

            },
             {
                 key: false, name: 'CaseTypeValue', index: 'CaseTypeValue', editable: true, edittype: 'select', formatter: 'select',
                 editoptions: { value: '1:Question;2:Problem;3:Request' }
             },

           {
               key: false, name: 'CaseStatusValue', index: 'CaseStatusValue', editable: true, edittype: 'select', formatter: 'select',
               editoptions: { value: '1:In Progress;2:On Hold;3:Waiting for Details;4:Reasearching' }
           },

            { key: false, name: 'Edit', search: false, index: 'Id', width: 30, sortable: false, formatter: editLink }
        ],
        pager: jQuery('#pager'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: 'Case List',
        emptyrecords: 'No records to display',
        formatter: 'actions',
        formatoptions: {
            keys: false,
            editbutton: true,
            delbutton: false,
            editformbutton: false,
            onSuccess: function (response) {
                if (response.status == 200) {
                }
            },
            extraparam: { oper: 'edit' }
        },
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#pager', { edit: false, add: false, del: false, search: false, refresh: false },
        {
            // edit  
            zIndex: 100,
            url: '/DynamicsCRMPortal/Case/UpdateCase',
            closeOnEscape: true,
            closeAfterEdit: true,
            recreateForm: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            // add  
            zIndex: 100,
            url: "/DynamicsCRMPortal/Case/Create",
            closeOnEscape: true,
            closeAfterAdd: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            // delete  
            zIndex: 100,
            url: "/DynamicsCRMPortal/Case/Delete",
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            msg: "Are you sure you want to delete this case?",
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        });
    $('#first_pager').prop('title', 'First');
    $('#prev_pager').prop('title', 'Previous');
    $('#last_pager').prop('title', 'Last');
    $('#next_pager').prop('title', 'Next');
});
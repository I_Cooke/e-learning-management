﻿using DynamicsCRMWebPortal.Models;
using JetBrains.Annotations;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;

namespace DynamicsCRMWebPortal.Handlers
{
    [UsedImplicitly]
    public class DynamicsCRMWebPortalSettingsPartHandler : ContentHandler
    {
        public DynamicsCRMWebPortalSettingsPartHandler(IRepository<DynamicsCRMWebPortalSettingsPartRecord> repository)
        {
            Filters.Add(new ActivatingFilter<DynamicsCRMWebPortalSettingsPart>("Site"));
            Filters.Add(StorageFilter.For(repository));
        }
    }
}
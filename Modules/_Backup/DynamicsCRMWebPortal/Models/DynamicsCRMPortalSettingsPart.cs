﻿using Orchard.ContentManagement;

namespace DynamicsCRMWebPortal.Models
{
    public class DynamicsCRMWebPortalSettingsPart : ContentPart<DynamicsCRMWebPortalSettingsPartRecord>
    {
        public string CRMConnectionString
        {
            get { return Record.CRMConnectionString; }
            set { Record.CRMConnectionString = value; }
        }
    }
}
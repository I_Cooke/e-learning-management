﻿using Orchard.ContentManagement.Records;

namespace DynamicsCRMWebPortal.Models
{
    public class DynamicsCRMWebPortalSettingsPartRecord : ContentPartRecord
    {
        public virtual string CRMConnectionString { get; set; }
    }
}
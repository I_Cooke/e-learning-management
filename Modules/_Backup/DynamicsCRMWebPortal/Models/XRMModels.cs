﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DynamicsCRMWebPortal.Models
{
    public class HomeModel
    {
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TicketNumber { get; set; }
    }

    public class ServiceCalendar
    {
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }

        public string constraint { get; set; }
        public bool overlap { get; set; }
        public string rendering { get; set; }
        public string color { get; set; }
        public string description { get; set; }
        //public string Customer { get; set; }
        //public string Resource { get; set; }
    }

    public class OpportunityModel
    {
        public string ContactId { get; set; }

        [Display(Name = "Contact")]
        public IEnumerable<SelectListItem> Contact { get; set; }

        [Required(ErrorMessage = "Please enter topic.")]
        [Display(Name = "Topic")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please select Purchase timeframe.")]
        [Display(Name = "Purchase Timeframe")]
        public string PurchaseTimeframe { get; set; }

        [Required(ErrorMessage = "Please enter Budget amount.")]
        [Display(Name = "Budget Amount")]
        public string BudgetAmount { get; set; }

        [Required(ErrorMessage = "Please select Purchase process.")]
        [Display(Name = "Purchase Process")]
        public string PurchaseProcess { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(2000, ErrorMessage = "This field cannot be longer than 2000 characters.")]
        public string Description { get; set; }

        public string OpportunityId { get; set; }

    }

    public class OpportunityUpdateModel
    {
        public string Id { get; set; }
        public string OpportunityId { get; set; }
        public string Name { get; set; }
        public string PurchaseTimeframe { get; set; }
        public string BudgetAmount { get; set; }
        public string PurchaseProcess { get; set; }
        public string Description { get; set; }

    }

    public class AccountModel
    {
        [Display(Name = "Email Address")]
        [StringLength(100, ErrorMessage = "This field cannot be longer than 100 characters.")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "This field cannot be longer than 50 characters.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage = "This field cannot be longer than 50 characters.")]
        public string LastName { get; set; }

        [Display(Name = "Address Name")]
        [StringLength(200, ErrorMessage = "This field cannot be longer than 200 characters.")]
        public string AddressName { get; set; }

        [Display(Name = "Address Line 1")]
        [StringLength(250, ErrorMessage = "This field cannot be longer than 250 characters.")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address Line 2")]
        [StringLength(250, ErrorMessage = "This field cannot be longer than 250 characters.")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Address Line 3")]
        [StringLength(250, ErrorMessage = "This field cannot be longer than 250 characters.")]
        public string AddressLine3 { get; set; }

        [Display(Name = "City")]
        [StringLength(80, ErrorMessage = "This field cannot be longer than 80 characters.")]
        public string City { get; set; }

        [Display(Name = "County")]
        [StringLength(50, ErrorMessage = "This field cannot be longer than 50 characters.")]
        public string County { get; set; }

        [Display(Name = "Country")]
        [StringLength(80, ErrorMessage = "This field cannot be longer than 80 characters.")]
        public string Country { get; set; }

        [Display(Name = "Post Code")]
        [StringLength(20, ErrorMessage = "This field cannot be longer than 80 characters.")]
        public string PostCode { get; set; }

        [Display(Name = "Telephone")]
        [StringLength(50, ErrorMessage = "This field cannot be longer than 50 characters.")]
        public string Telephone { get; set; }
    }

    public class CustomerModel
    {
        public string CustomerId { get; set; }

        [Display(Name = "Email Address")]
        [StringLength(100, ErrorMessage = "This field cannot be longer than 100 characters.")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "This field cannot be longer than 50 characters.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage = "This field cannot be longer than 50 characters.")]
        public string LastName { get; set; }

        [Display(Name = "Address Name")]
        [StringLength(200, ErrorMessage = "This field cannot be longer than 200 characters.")]
        public string AddressName { get; set; }

        [Display(Name = "Address Line 1")]
        [StringLength(250, ErrorMessage = "This field cannot be longer than 250 characters.")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address Line 2")]
        [StringLength(250, ErrorMessage = "This field cannot be longer than 250 characters.")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Address Line 3")]
        [StringLength(250, ErrorMessage = "This field cannot be longer than 250 characters.")]
        public string AddressLine3 { get; set; }

        [Display(Name = "City")]
        [StringLength(80, ErrorMessage = "This field cannot be longer than 80 characters.")]
        public string City { get; set; }

        [Display(Name = "County")]
        [StringLength(50, ErrorMessage = "This field cannot be longer than 50 characters.")]
        public string County { get; set; }

        [Display(Name = "Country")]
        [StringLength(80, ErrorMessage = "This field cannot be longer than 80 characters.")]
        public string Country { get; set; }

        [Display(Name = "Post Code")]
        [StringLength(20, ErrorMessage = "This field cannot be longer than 80 characters.")]
        public string PostCode { get; set; }

        [Display(Name = "Telephone")]
        [StringLength(50, ErrorMessage = "This field cannot be longer than 50 characters.")]
        public string Telephone { get; set; }
    }

    public class OrderModel
    {
        public string OrderId { get; set; }
        public string CustomerId { get; set; }

        [Display(Name = "Customers")]
        public IEnumerable<SelectListItem> Customers { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Order Number")]
        public string OrderNumber { get; set; }

        [Display(Name = "Order Status")]
        public string OrderStatus { get; set; }

        [Required]
        [Display(Name = "Order Detail")]
        [StringLength(2000, ErrorMessage = "This field cannot be longer than 2000 characters.")]
        public string Description { get; set; }
    }

    public class OrderUpdateModel
    {
        public string OrderId { get; set; }
        public string CustomerId { get; set; }

        [Display(Name = "Customers")]
        public IEnumerable<SelectListItem> Customers { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Order Number")]
        public string OrderNumber { get; set; }

        [Display(Name = "Order Status")]
        public string OrderStatus { get; set; }

        [Display(Name = "Order Detail")]
        [StringLength(2000, ErrorMessage = "This field cannot be longer than 2000 characters.")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Note")]
        [StringLength(2000, ErrorMessage = "This field cannot be longer than 2000 characters.")]
        public string Note { get; set; }

        [Display(Name = "Attachment")]
        public string NoteAttachment { get; set; }
    }

    public class CaseModel
    {
        [Required]
        [Display(Name = "Title")]
        [StringLength(200, ErrorMessage = "This field cannot be longer than 200 characters.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please select a case type.")]
        [Display(Name = "Case Type")]
        public string CaseType { get; set; }

        [Required]
        [Display(Name = "Case Detail")]
        [StringLength(2000, ErrorMessage = "This field cannot be longer than 2000 characters.")]
        public string Description { get; set; }
    }

    public class CaseUpdateModel
    {
        public string IncidentId { get; set; }
        public string TicketNumber { get; set; }
        public string Title { get; set; }
        public string CaseStatus { get; set; }
        public string CaseType { get; set; }
        public string Description { get; set; }

        
        [Display(Name = "Note")]
        [StringLength(2000, ErrorMessage = "This field cannot be longer than 2000 characters.")]
        public string Note { get; set; }

        [Display(Name = "Attachment")]
        public string NoteAttachment { get; set; }
    }

    public class ArticleModel
    {
        public string Title { get; set; }
        public IEnumerable<SelectListItem> Subject { get; set; }
        public string SubjectItemId { get; set; }
        public string ArticleData { get; set; }
    }

    public class CalendarModel
    {
        [Required]
        public string Subject { get; set; }
        public IEnumerable<SelectListItem> Resource { get; set; }
        [Required]
        public string ResourceItemId { get; set; }
        [Required]
        public DateTime StartTime { get; set; }
        public List<String> OccupiedDates { get; set; }
    }
}
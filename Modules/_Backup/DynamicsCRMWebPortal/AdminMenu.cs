﻿using Orchard.Localization;
using Orchard.Security;
using Orchard.UI.Navigation;

namespace DynamicsCRMWebPortal
{
    public class AdminMenu : INavigationProvider
    {
        public string MenuName
        {
            get { return "admin"; }
        }

        public AdminMenu()
        {
            T = NullLocalizer.Instance;
        }

        private Localizer T { get; set; }

        public void GetNavigation(NavigationBuilder builder)
        {
            builder.Add(item => item.Caption(T("Dynamics CRM Web Portal")).Position("13").Action("Index", "Admin", new { area = "DynamicsCRMWebPortal" }).Permission(StandardPermissions.SiteOwner));
        }
    }
}
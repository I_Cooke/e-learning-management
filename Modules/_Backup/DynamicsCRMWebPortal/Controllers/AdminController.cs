﻿using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Core.Contents.Controllers;
using Orchard.Localization;
using Orchard.Security;
using Orchard.UI.Admin;
using Orchard.UI.Notify;

namespace DynamicsCRMWebPortal.Controllers {
    [ValidateInput(false), Admin]
    public class AdminController : Controller {
        public AdminController(IOrchardServices services) {
            Services = services;
            T = NullLocalizer.Instance;
        }

        public IOrchardServices Services { get; set; }
        public Localizer T { get; set; }

        public ActionResult Index() {
            if (!Services.Authorizer.Authorize(StandardPermissions.SiteOwner, T("Not authorized to manage settings"))) {
                return new HttpUnauthorizedResult();
            }

            var crmSettingsPart = Services.WorkContext.CurrentSite.As<DynamicsCRMWebPortalSettingsPart>();

            var viewModel = new DynamicsCRMWebPortalViewModel {
                Settings = crmSettingsPart
            };

            return View(viewModel);
        }

        [Orchard.Mvc.FormValueRequired("submit")]
        [HttpPost, ActionName("Index")]
        public ActionResult IndexPost() {
            if (!Services.Authorizer.Authorize(StandardPermissions.SiteOwner, T("Not authorized to manage settings"))) {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new DynamicsCRMWebPortalViewModel {
                Settings = Services.WorkContext.CurrentSite.As<DynamicsCRMWebPortalSettingsPart>(),
            };

            TryUpdateModel(viewModel);

            if (ModelState.IsValid) {
                Services.Notifier.Information(T("Dynamics CRM settings updated successfully."));
            }
            else {
                Services.TransactionManager.Cancel();
            }

            return Index();
        }
    }
}
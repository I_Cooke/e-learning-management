﻿using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.Services;
using Orchard;
using Orchard.Themes;
using Xrm;

namespace DynamicsCRMWebPortal.Controllers {
    [Themed]
    public class AccountController : Controller {
        private const string HomeUrl = "~/DynamicsCRMWebPortal";
        private readonly IXrmRepository _repository;

        public AccountController(IOrchardServices orchardServices) {
            _repository = new XrmRepository(orchardServices);
        }

        public AccountController(IXrmRepository repository) {
            _repository = repository;
        }

        [Authorize]
        public ActionResult Index() {
            var crmUser = _repository.RetrieveMyInfoByIdentifier();
            if (crmUser != null) {
                var model = new AccountModel {
                    EmailAddress = crmUser.EMailAddress1,
                    FirstName = crmUser.FirstName,
                    LastName = crmUser.LastName,
                    AddressName = crmUser.Address1_Name,
                    AddressLine1 = crmUser.Address1_Line1,
                    AddressLine2 = crmUser.Address1_Line2,
                    AddressLine3 = crmUser.Address1_Line3,
                    City = crmUser.Address1_City,
                    County = crmUser.Address1_County,
                    Country = crmUser.Address1_Country,
                    PostCode = crmUser.Address1_PostalCode,
                    Telephone = crmUser.Address1_Telephone1
                };

                return View(model);
            }

            TempData["UserMessage"] = "You don't have a CRM account yet - please create your CRM account.";
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AccountModel model, string button) {
            if (button == "cancel") {
                return Redirect(HomeUrl);
            }

            if (ModelState.IsValid) {
                //Create a new contact
                var crmUser = new Contact {
                    EMailAddress1 = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Address1_Name = model.AddressName,
                    Address1_Line1 = model.AddressLine1,
                    Address1_Line2 = model.AddressLine2,
                    Address1_Line3 = model.AddressLine3,
                    Address1_City = model.City,
                    Address1_County = model.County,
                    Address1_Country = model.Country,
                    Address1_PostalCode = model.PostCode,
                    Address1_Telephone1 = model.Telephone
                };

                _repository.CreateOrUpdateMyInfo(crmUser);
                TempData["UserMessage"] = "CRM profile has been updated.";

                return View(model);
            }

            ModelState.AddModelError("", "CRM profile update error.");
            return View(model);
        }
    }
}
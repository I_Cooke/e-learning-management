﻿using System;
using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.Services;
using Orchard;
using Orchard.Themes;
using Xrm;

namespace DynamicsCRMWebPortal.Controllers {
    [Themed]
    public class HomeController : Controller {
        private const string HomeUrl = "~/DynamicsCRMWebPortal/Home";
        private const string RelUrl = "~/DynamicsCRMWebPortal/Account";
        private static Guid _crmUserId = Guid.Empty;
        private readonly IXrmRepository _repository;

        public HomeController(IOrchardServices orchardServices) {
            _repository = new XrmRepository(orchardServices);
        }

        public HomeController(IXrmRepository repository) {
            _repository = repository;
        }

        [Authorize]
        public ActionResult Index() {
            if (!_repository.IsDisposed()) {
                var crmUser = _repository.RetrieveMyInfoByIdentifier();
                if (crmUser != null) {
                    var model = new HomeModel {
                        FirstName = crmUser.FirstName,
                        LastName = crmUser.LastName,
                        EmailAddress = crmUser.EMailAddress1
                    };
                    return View(model);
                }

                TempData["UserMessage"] = "You don't have a CRM account yet - please create your CRM account.";
                return Redirect(RelUrl);
            }

            TempData["UserMessage"] = "Please configure the Microsoft Dynamics CRM connection string.";
            return Redirect(HomeUrl);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(HomeModel model) {
            return View();
        }
    }
}
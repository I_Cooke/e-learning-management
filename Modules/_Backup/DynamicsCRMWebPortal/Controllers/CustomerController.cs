﻿using System;
using System.Linq;
using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.Services;
using Microsoft.Xrm.Client;
using Orchard;
using Orchard.Themes;
using Xrm;
using System.Globalization;

namespace DynamicsCRMWebPortal.Controllers
{
    [Themed]
    public class CustomerController : Controller
    {
        private const string HomeUrl = "~/DynamicsCRMWebPortal/Home";
        private const string RelUrl = "~/DynamicsCRMWebPortal/Customer";
        private static Guid _crmUserId = Guid.Empty;
        private readonly IXrmRepository _repository;

        public CustomerController(IOrchardServices orchardServices)
        {
            _repository = new XrmRepository(orchardServices);
        }

        public CustomerController(IXrmRepository repository)
        {
            _repository = repository;
        }

        public JsonResult GetCustomers(string sidx, string sord, int page, int rows)  //Gets the todo Lists.
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            ////Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            var customers = _repository.RetrieveCustomers(_crmUserId, Guid.Empty, false);

            var customersList = customers.Select(
                a => new
                {
                    a.Id,
                    a.FullName,
                    a.FirstName,
                    a.LastName,
                    a.Address1_Name, 
                    a.Address1_Line1,
                    a.Address1_Line2,
                    a.Address1_Line3,
                    a.Address1_City,
                    a.Address1_County,
                    a.Address1_Country,
                    a.EMailAddress1,
                    a.Address1_Telephone1,
                    a.Address1_PostalCode,
                    a.ModifiedOn
                });
            int totalRecords = customersList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                customersList = customersList.OrderByDescending(s => s.ModifiedOn);
                customersList = customersList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                customersList = customersList.OrderBy(s => s.ModifiedOn);
                customersList = customersList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = customersList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult Index()
        { //Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();
            if (_crmUserId != Guid.Empty)
            {
                return View();
            }

            TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;
            return Redirect(HomeUrl);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(CustomerModel model)
        {
            string strMsg = string.Empty;

            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            if (_crmUserId != Guid.Empty)
            {
                //Create a new contact
                var customer = new Contact
                {
                    ParentCustomerId = new CrmEntityReference("contact", _crmUserId),
                    EMailAddress1 = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Address1_Name = model.AddressName,
                    Address1_Line1 = model.AddressLine1,
                    Address1_Line2 = model.AddressLine2,
                    Address1_Line3 = model.AddressLine3,
                    Address1_City = model.City,
                    Address1_County = model.County,
                    Address1_Country = model.Country,
                    Address1_PostalCode = model.PostCode,
                    Address1_Telephone1 = model.Telephone
                };

                _repository.CreateCrmRecord(customer);
                TempData["UserMessage"] = Properties.Settings.Default.CreateCustomerMsg;
            }
            else
            {
                TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;
            }

            //ModelState.AddModelError("", "CRM customer create error.");
            return Json(TempData["UserMessage"], JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Update(CustomerModel model)
        { 
            //Retrieve the customer
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            //Get the customer
            if (_crmUserId != Guid.Empty)
            {
                //Create a new customer
                var customer = new Contact
                {
                    ContactId = new Guid(model.CustomerId),
                    EMailAddress1 = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Address1_Name = model.AddressName,
                    Address1_Line1 = model.AddressLine1,
                    Address1_Line2 = model.AddressLine2,
                    Address1_Line3 = model.AddressLine3,
                    Address1_City = model.City,
                    Address1_County = model.County,
                    Address1_Country = model.Country,
                    Address1_PostalCode = model.PostCode,
                    Address1_Telephone1 = model.Telephone
                };

                _repository.UpdateCrmRecord(customer);

                TempData["UserMessage"] = Properties.Settings.Default.CustomerUpdateMsg;
            }
            else
            {
                TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;
            } 
            return Json(TempData["UserMessage"], JsonRequestBehavior.AllowGet);
        }
    }
}
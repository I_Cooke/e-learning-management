﻿using System;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.Services;
using Microsoft.Xrm.Client;
using Orchard;
using Orchard.Themes;
using Xrm;

namespace DynamicsCRMWebPortal.Controllers
{
    [Themed]
    public class OpportunityController : Controller
    {
        private const string HomeUrl = "~/DynamicsCRMWebPortal/Home";
        private const string RelUrl = "~/DynamicsCRMWebPortal/Opportunity";
        private static Guid _crmUserId = Guid.Empty;
        private readonly IXrmRepository _repository;

        public OpportunityController(IOrchardServices orchardServices)
        {
            _repository = new XrmRepository(orchardServices);
        }

        public OpportunityController(IXrmRepository repository)
        {
            _repository = repository;
        }
        public JsonResult GetOpportunities(string sidx, string sord, int page, int rows)  //Gets the todo Lists.
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            ////Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            var opportunities = _repository.RetrieveOpportunities(_crmUserId, null);

            var opportunitiesList = opportunities.Select(
                    a => new
                    {
                        a.Id,
                        a.Name,
                        a.BudgetAmount,
                        a.Description,
                        a.PurchaseProcess,
                        a.PurchaseTimeFrame
                    });
            int totalRecords = opportunitiesList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                opportunitiesList = opportunitiesList.OrderByDescending(s => s.Name);
                opportunitiesList = opportunitiesList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                opportunitiesList = opportunitiesList.OrderBy(s => s.Name);
                opportunitiesList = opportunitiesList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = opportunitiesList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult Index()
        {
            ////Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            //Get a list of cases
            if (_crmUserId != Guid.Empty)
            {
                var model = new OpportunityModel();
                var contact = _repository.RetrieveCustomers(_crmUserId, Guid.Empty, true);
                if (contact != null && contact.Any())
                {
                    model.Contact = new SelectList(contact, "ID", "FullName");
                }
                return View(model);
            }
            TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;
            return Redirect(RelUrl);
        }

        //[Authorize]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Index(OpportunityModel model)
        //{
        //    return View(model);
        //}

        //[Authorize]
        //public ActionResult Create()
        //{
        //    //Retrieve the current contact
        //    _crmUserId = _repository.RetrieveMyIdByIdentifier();
        //    if (_crmUserId != Guid.Empty)
        //    {
        //        var model = new OpportunityModel();
        //        var contact = _repository.RetrieveCustomers(_crmUserId, Guid.Empty, true);
        //        if (contact != null && contact.Any())
        //        {
        //            model.Contact = new SelectList(contact, "ID", "FullName");
        //        }

        //        return View(model);
        //    }


        //    TempData["UserMessage"] = Properties.Resources.CRMAccountErrorMsg;
        //    return Redirect(HomeUrl);
        //} 

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(OpportunityUpdateModel model)
        {
            string msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    _crmUserId = _repository.RetrieveMyIdByIdentifier();
                    if (_crmUserId != Guid.Empty)
                    {
                        ////Create a new Opportunity
                        var opportunity = new Opportunity
                        {
                            Name = model.Name,
                            PurchaseTimeframe = Convert.ToInt32(model.PurchaseTimeframe),
                            PurchaseProcess = Convert.ToInt32(model.PurchaseProcess),
                            BudgetAmount = Convert.ToDecimal(model.BudgetAmount),
                            Description = model.Description,
                            //ParentContactId = new CrmEntityReference("contact", new Guid(model.ContactId))
                        };
                        _repository.CreateCrmRecord(opportunity);
                        msg = Properties.Settings.Default.CreateOpportunityMsg;
                    }
                    else
                    {
                        msg = Properties.Settings.Default.CRMAccountErrorMsg;
                    }
                }
                else
                {
                    msg = Properties.Settings.Default.ModelValidationError;
                }
            }
            catch (Exception ex)
            {
                msg = Properties.Settings.Default.ErrorMsg + ex.Message;
            }
           // TempData["UserMessage"] = msg;
            return Json(msg, JsonRequestBehavior.AllowGet); // Json(msg, JsonRequestBehavior.AllowGet); 
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Update(OpportunityUpdateModel model)//, string button)
        {
            string msg = string.Empty;

            if (ModelState.IsValid)
            {
                //Retrieve the customer
                _crmUserId = _repository.RetrieveMyIdByIdentifier();

                //Get the customer
                if (_crmUserId != Guid.Empty)
                {
                    //Create a new opportunity
                    var opportunity = new Opportunity
                    {
                        OpportunityId = new Guid(model.OpportunityId),
                        Name = model.Name,
                        PurchaseTimeframe = Convert.ToInt32(model.PurchaseTimeframe),
                        BudgetAmount = Convert.ToDecimal(model.BudgetAmount),
                        PurchaseProcess = Convert.ToInt32(model.PurchaseProcess),
                        Description = model.Description

                    };

                    _repository.UpdateCrmRecord(opportunity);
                    msg = Properties.Settings.Default.UpdatedMsg;


                }
                else
                {
                    msg = Properties.Settings.Default.CRMAccountErrorMsg;
                }

            }
            else
            {
                msg = Properties.Settings.Default.ModelValidationError;
            }
            //TempData["UserMessage"] = msg;
            return Json(msg, JsonRequestBehavior.AllowGet); // Json(msg, JsonRequestBehavior.AllowGet); 
        }

    }
}
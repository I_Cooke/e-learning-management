﻿using System;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.Services;
using Microsoft.Xrm.Client;
using Orchard;
using Orchard.Themes;
using Xrm;

namespace DynamicsCRMWebPortal.Controllers
{
    [Themed]
    public class OrderController : Controller
    {
        private const string HomeUrl = "~/DynamicsCRMWebPortal/Home";
        private const string RelUrl = "~/DynamicsCRMWebPortal/Order";
        private static Guid _crmUserId = Guid.Empty;
        private readonly IXrmRepository _repository;

        public OrderController(IOrchardServices orchardServices)
        {
            _repository = new XrmRepository(orchardServices);
        }

        public OrderController(IXrmRepository repository)
        {
            _repository = repository;
        }
        public JsonResult GetOrders(string sidx, string sord, int page, int rows)  //Gets the todo Lists.
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            ////Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            var orders = _repository.RetrieveCustomerOrders(_crmUserId, null);

            var ordreList = orders.Select(
                a => new
                {
                    a.Id,
                    a.OrderNumber,
                    a.OrderStatus,
                    a.Description,
                    a.CustomerName
                });
            int totalRecords = ordreList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                ordreList = ordreList.OrderByDescending(s => s.OrderNumber);
                ordreList = ordreList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                ordreList = ordreList.OrderBy(s => s.OrderNumber);
                ordreList = ordreList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ordreList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Index()
        {
            OrderModel model = new OrderModel();
            //Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            //Get a list of cases
            if (_crmUserId != Guid.Empty)
            {
                var customers = _repository.RetrieveCustomers(_crmUserId, Guid.Empty, true);
                if (customers != null && customers.Any())
                {
                    model.Customers = new SelectList(customers, "ID", "FullName");
                }
                return View(model);
            }

            TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;
            return Redirect(HomeUrl);
        }


         
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(OrderModel model)
        { 
            _crmUserId = _repository.RetrieveMyIdByIdentifier(); 

            if (_crmUserId != Guid.Empty)
            {
                //Create a new order
                var order = new SalesOrder
                {
                    //TransactionCurrencyId = new CrmEntityReference("transactioncurrency", new Guid("{E6C2E2CA-A459-E211-9409-A0B3CCDF1518}")),
                    CustomerId = new CrmEntityReference("contact", new Guid(model.CustomerId)),
                    Description = model.Description
                };
                _repository.CreateCrmRecord(order);

                TempData["UserMessage"] = Properties.Settings.Default.OrderSuccMsg;

            }
            else
            {
                TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;
            }

            return Json(TempData["UserMessage"], JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Update(string orderNumber)
        {
            //Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            //Get the order
            if (_crmUserId != Guid.Empty)
            {
                var model = new OrderUpdateModel();
                if (orderNumber != null)
                {
                    var orders = _repository.RetrieveCustomerOrders(_crmUserId, orderNumber);
                    if (orders != null && orders.Count() == 1)
                    {
                        var order = orders.SingleOrDefault();

                        if (order != null)
                        {
                            model.OrderId = order.Id;
                            model.OrderStatus = order.OrderStatus;
                            model.CustomerName = order.CustomerName;
                            model.Description = order.Description;

                            var notes = _repository.RetrieveNotes(new Guid(order.Id), Guid.Empty);
                            if (notes != null && notes.Any())
                            {
                                ViewData["notes"] = notes;
                            }
                        }
                    }
                }

                return View(model);
            }

            TempData["UserMessage"] = "You don't have a CRM account yet - please create your CRM account.";
            return Redirect(HomeUrl);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(OrderUpdateModel model, string button, HttpPostedFileBase file)
        {
            if (button == "cancel")
            {
                return Redirect(RelUrl);
            }

            if (ModelState.IsValid)
            {
                //Retrieve the current contact
                _crmUserId = _repository.RetrieveMyIdByIdentifier();

                if (_crmUserId != Guid.Empty)
                {
                    //Update order
                    if (model.OrderStatus == "New" && model.OrderId != null)
                    {
                        var order = new SalesOrder
                        {
                            SalesOrderId = new Guid(model.OrderId),
                            Description = model.Description
                        };

                        _repository.UpdateCrmRecord(order);
                    }

                    //Create note
                    if (model.OrderId != null)
                    {
                        _repository.CreateNote(model.Note, new Guid(model.OrderId), "salesorder", file);
                    }
                    TempData["UserMessage"] = "The order has been updated.";

                    return Redirect(RelUrl);
                }

                TempData["UserMessage"] = "You don't have a CRM account yet - please create your CRM account.";
                return Redirect(HomeUrl);
            }

            ModelState.AddModelError("", "CRM order update error.");
            return View(model);
        }

        [Authorize]
        public ActionResult NoteAttachment(string incidentId, string noteId)
        {
            if (incidentId != null && noteId != null)
            {
                var notes = _repository.RetrieveNotes(new Guid(incidentId), new Guid(noteId));

                if (notes != null && notes.Count() == 1)
                {
                    var note = notes.SingleOrDefault();

                    if (note != null)
                    {
                        var noteBody = Convert.FromBase64String(note.DocumentBody);
                        return File(noteBody, MediaTypeNames.Application.Octet, note.FileName);
                    }
                }
            }

            return View();
        }
    }
}
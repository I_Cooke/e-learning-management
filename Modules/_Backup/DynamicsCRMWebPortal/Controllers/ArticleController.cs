﻿using System;
using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.Services;
using Orchard;
using Orchard.Themes;
using Xrm;

namespace DynamicsCRMWebPortal.Controllers {
    [Themed]
    public class ArticleController : Controller {
        private const string RelUrl = "~/DynamicsCRMWebPortal/Article";
        private readonly IXrmRepository _repository;

        public ArticleController(IOrchardServices orchardServices) {
            _repository = new XrmRepository(orchardServices);
        }

        public ArticleController(IXrmRepository repository) {
            _repository = repository;
        }

        [Authorize]
        public ActionResult Index(string subjectId) {
            //Retrieve articles
            var articles = _repository.RetrieveArticlesBySubjectId(subjectId != null ? new Guid(subjectId) : Guid.Empty);

            //Get a list of cases
            if (articles != null) {
                ViewData["articles"] = _repository.ListArticles(articles);
            }

            var model = new ArticleModel {Subject = new SelectList(_repository.RetrieveSubjectByParent("Articles"), "ID", "Title")};
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ArticleModel model, string button) {
            if (button != "select") {
                return Redirect(RelUrl);
            }

            if (ModelState.IsValid) {
                Index(model.SubjectItemId);
                return View();
            }

            ModelState.AddModelError("", "CRM article retrieve error.");
            return View();
        }

        [Authorize]
        public ActionResult Open(string articleNumber) {
            if (articleNumber != null) {
                var article = (KbArticle) _repository.RetrieveCrmRecord("kbarticle", "number", articleNumber);

                if (article != null && article.StatusCode != null && article.StatusCode.Value == 3) {
                    ViewData["article"] = article;
                }
            }

            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Open(ArticleModel model, string button) {
            if (button == "ok") {
                return Redirect(RelUrl);
            }

            if (ModelState.IsValid) {
                return View(model);
            }

            ModelState.AddModelError("", "CRM article open error.");
            return View();
        }
    }
}
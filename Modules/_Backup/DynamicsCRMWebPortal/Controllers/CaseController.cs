﻿using System;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.Services;
using Microsoft.Xrm.Client;
using Orchard;
using Orchard.Themes;
using Xrm;

namespace DynamicsCRMWebPortal.Controllers
{
    [Themed]
    public class CaseController : Controller
    {
        private const string HomeUrl = "~/DynamicsCRMWebPortal/Home";
        private const string RelUrl = "~/DynamicsCRMWebPortal/Case";
        private static Guid _crmUserId = Guid.Empty;
        private readonly IXrmRepository _repository;

        public CaseController(IOrchardServices orchardServices)
        {
            _repository = new XrmRepository(orchardServices);
        }

        public CaseController(IXrmRepository repository)
        {
            _repository = repository;
        }

        public JsonResult GetCases(string sidx, string sord, int page, int rows)  //Gets the todo Lists.
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            ////Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            var cases = _repository.RetrieveIncidents(_crmUserId, null);

            var casesList = cases.Select(
                a => new
                {
                    a.Id,
                    a.Title,
                    a.Description,
                    a.TicketNumber,
                    a.CaseTypeValue,
                    a.CaseStatusValue

                });
            int totalRecords = casesList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                casesList = casesList.OrderByDescending(s => s.Title);
                casesList = casesList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                casesList = casesList.OrderBy(s => s.Title);
                casesList = casesList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = casesList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult Index()
        {
            ////Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            //Get a list of cases
            if (_crmUserId != Guid.Empty)
            {
                return View();
            }
          TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;

            return Redirect(RelUrl);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(CaseModel model)
        {
            string msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    if (_crmUserId != Guid.Empty)
                    {
                        //Create a new incident
                        var incident = new Incident
                        {
                            Title = model.Title,
                            CaseTypeCode = Convert.ToInt32(model.CaseType),
                            CustomerId = new CrmEntityReference("contact", _crmUserId),
                            Description = model.Description
                        };
                        _repository.CreateCrmRecord(incident);
                        msg = Properties.Settings.Default.CreateCaseMsg;
                    }
                    else
                    {
                        msg = Properties.Settings.Default.CRMAccountErrorMsg;
                    }
                }
                else
                {
                    msg = Properties.Settings.Default.ModelValidationError;
                }
            }
            catch (Exception ex)
            {
                msg = Properties.Settings.Default.ErrorMsg + ex.Message;
            }
            //TempData["UserMessage"] = msg;
            return Json(msg, JsonRequestBehavior.AllowGet); // Json(msg, JsonRequestBehavior.AllowGet); 
        }

        [Authorize] 
        public JsonResult Update(string ticketNumber)
        {
            //Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            var jsonData = new Object();
            //Get the cases
            if (_crmUserId != Guid.Empty)
            {

                if (ticketNumber != null)
                {
                    var incidents = _repository.RetrieveIncidents(_crmUserId, ticketNumber);
                    if (incidents != null && incidents.Count() == 1)
                    {
                        var incident = incidents.SingleOrDefault();

                        var notes = _repository.RetrieveNotes(new Guid(incident.Id), Guid.Empty);
                        if (notes != null)
                        {
                            jsonData = new
                            {
                                NotesData = notes
                            };

                        }

                    }

                    
                }
                return Json(jsonData, JsonRequestBehavior.AllowGet);
             }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }

            //TempData["UserMessage"] = "You don't have a CRM account yet - please create your CRM account.";
            //return Redirect(HomeUrl);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Update(CaseUpdateModel model, HttpPostedFileBase file)
        {
            string msg = string.Empty;
            if (ModelState.IsValid)
            {
                //Retrieve the current contact
                _crmUserId = _repository.RetrieveMyIdByIdentifier();
               
                if (_crmUserId != Guid.Empty)
                {
                    
                    var updateCase = new Incident
                    {
                        IncidentId = new Guid(model.IncidentId),
                        CaseTypeCode = Convert.ToInt32(model.CaseType),
                        Title = model.Title,
                        Description = model.Description 
                    };
                    //Create note
                    if( !string.IsNullOrEmpty(model.Note))
                    {
                        _repository.CreateNote(model.Note, new Guid(model.IncidentId), "incident", file); 
                    }
                       _repository.UpdateCrmRecord(updateCase);
                    msg = Properties.Settings.Default.UpdateCaseMsg;
                }

                else
                {
                    msg = Properties.Settings.Default.CRMAccountErrorMsg;
                }
            }
            else
            {
                msg = Properties.Settings.Default.ModelValidationError;
            }

          //  TempData["UserMessage"] = msg;
            return Json(msg, JsonRequestBehavior.AllowGet); // Json(msg, JsonRequestBehavior.AllowGet); 
        }


        [Authorize]
        public ActionResult NoteAttachment(string incidentId, string noteId)
        {
            if (incidentId != null && noteId != null)
            {
                var notes = _repository.RetrieveNotes(new Guid(incidentId), new Guid(noteId));

                if (notes != null && notes.Count() == 1)
                {
                    var note = notes.SingleOrDefault();

                    if (note != null)
                    {
                        var noteBody = Convert.FromBase64String(note.DocumentBody);
                        return File(noteBody, MediaTypeNames.Application.Octet, note.FileName);
                    }
                }
            }

            return View();
        }


   
    }
}
﻿using System;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using DynamicsCRMWebPortal.Models;
using DynamicsCRMWebPortal.Services;
using Microsoft.Xrm.Client;
using Orchard;
using Orchard.Themes;
using Xrm;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using System.Globalization;


namespace DynamicsCRMWebPortal.Controllers
{
    [Themed]
    public class CalendarController : Controller
    {
        private const string HomeUrl = "~/DynamicsCRMWebPortal/Home";
        private const string RelUrl = "~/DynamicsCRMWebPortal/Calendar";
        private static Guid _crmUserId = Guid.Empty;
        private readonly IXrmRepository _repository;
        static string[] SUPPORTED_DATE_FORMATS = { "dd/MM/yyyy", "MM/dd/yyyy", "dd/MMM/yyyy", "dd/MM/yy", "yy/MM/dd", "yy-MM-dd", "M/d/yyyy hh:mm tt", "M/dd/yyyy hh:mm tt", "MM/d/yyyy hh:mm tt", "MM/dd/yyyy hh:mm tt" };

        public CalendarController(IOrchardServices orchardServices)
        {
            _repository = new XrmRepository(orchardServices);
        }

        public CalendarController(IXrmRepository repository)
        {
            _repository = repository;
        }

        public JsonResult GetCalendarData()  //Gets the todo Lists.
        {
            ServiceCalendar serCalen = null;

            List<ServiceCalendar> lstSerCal = new List<ServiceCalendar>();
            //Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();
            DateTime scheduledstart = DateTime.MinValue;
            //Get a list of service calendar items
            if (_crmUserId != Guid.Empty)
            {
                var appointments = _repository.RetrieveAppointmentByParty(_crmUserId);
                for (int i = 0; i < appointments.Count; i++)
                {
                    serCalen = new ServiceCalendar();
                    serCalen.title = appointments[i].Subject;
                    serCalen.description = appointments[i].Subject;
                    if (appointments[i].FormattedValues.Contains("scheduledstart") == true)
                    {
                        if (appointments[i].FormattedValues["scheduledstart"] != null && appointments[i].FormattedValues["scheduledstart"] != string.Empty)
                        {
                            string[] strArrDate = appointments[i].FormattedValues["scheduledstart"].Split(' ')[0].Split('/');
                            if (strArrDate.Length >= 3)
                            {
                                serCalen.start = Convert.ToInt32(strArrDate[2]) + " " + Convert.ToInt32(strArrDate[0]) + " " + Convert.ToInt32(strArrDate[1]); //appointments[i].ScheduledStart.Value.ToLocalTime().Date;

                                serCalen.end = serCalen.start;// new DateTime(Convert.ToInt32(strArrDate[2]), Convert.ToInt32(strArrDate[0]), Convert.ToInt32(strArrDate[1])); //appointments[i].ScheduledStart.Value.ToLocalTime().Date;
                            }
                        }
                    }
                    //if (appointments[i].FormattedValues.Contains("scheduledend") == true)
                    //{
                    //    serCalen.end = ConvertToDateTime(appointments[i].FormattedValues["scheduledend"]);
                    //}
                    serCalen.overlap = false;
                    serCalen.color = Properties.Settings.Default.DefaultColor;
                    lstSerCal.Add(serCalen);
                }
                return Json(lstSerCal, JsonRequestBehavior.AllowGet);
            }
            else //create a new CRM account
            {
                TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;
                return Json(null, JsonRequestBehavior.AllowGet);
            }

        }
        [Authorize]
        public ActionResult Index()
        {
            //Retrieve the current contact
            _crmUserId = _repository.RetrieveMyIdByIdentifier();

            //Get a list of service calendar items
            if (_crmUserId != Guid.Empty)
            {

                CalendarModel model = new CalendarModel();

                //var resources = _repository.RetrieveResourceByObjectType("equipment");
                var resources = _repository.RetrieveResourceByObjectType("systemuser");
                if (resources != null && resources.Count() > 0)
                {
                    model.Resource = new SelectList(resources, "ID", "Name");
                }

                var occupiedDates = _repository.RetrieveAppointmentOccupiedDateByResource(Guid.Empty);
                if (occupiedDates != null && occupiedDates.Count() > 0)
                {
                    model.OccupiedDates = occupiedDates;
                }

                return View(model);
            }
            else //create a new CRM account
            {
                TempData["UserMessage"] = Properties.Settings.Default.CRMAccountErrorMsg;
                return Redirect(HomeUrl);
            }
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CalendarModel model)
        {
            return View(model);
        }

        //[Authorize]
        //public ActionResult Create(string resourceId)
        //{
        //    //Retrieve the current contact
        //    _crmUserId = _repository.RetrieveMyIdByIdentifier();

        //    //Get a list of resources
        //    if (_crmUserId != Guid.Empty)
        //    {
        //        CalendarModel model = new CalendarModel();

        //        //var resources = _repository.RetrieveResourceByObjectType("equipment");
        //        var resources = _repository.RetrieveResourceByObjectType("systemuser");
        //        if (resources != null && resources.Count() > 0)
        //        {
        //            model.Resource = new SelectList(resources, "ID", "Name");
        //        }

        //        var occupiedDates = _repository.RetrieveAppointmentOccupiedDateByResource(resourceId != null ? new Guid(resourceId) : Guid.Empty);
        //        if (occupiedDates != null && occupiedDates.Count() > 0)
        //        {
        //            model.OccupiedDates = occupiedDates;
        //        }

        //        return View(model);
        //    }
        //    else //create a new CRM account
        //    {
        //        TempData["UserMessage"] = "You don't have a CRM account yet - please create your CRM account.";
        //        return Redirect(HomeUrl);
        //    }
        //}

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(CalendarModel model)
        {
            string strMsg = string.Empty;



            if (_crmUserId != Guid.Empty)
            {
                if (model.ResourceItemId != null)
                {
                    ActivityParty customer = new ActivityParty
                    {
                        PartyId = new EntityReference(Contact.EntityLogicalName, _crmUserId)
                    };

                    ActivityParty resource = new ActivityParty
                    {
                        //PartyId = new EntityReference(Equipment.EntityLogicalName, new Guid(model.ResourceItemId))
                        PartyId = new EntityReference(SystemUser.EntityLogicalName, new Guid(model.ResourceItemId))
                    };

                    //Create a new service appointment
                    var appointment = new Xrm.ServiceAppointment
                    {
                        Subject = model.Subject,
                        ScheduledStart = model.StartTime.Date,
                        ScheduledEnd = model.StartTime.Date.AddHours(23).AddMinutes(59),
                        Customers = new ActivityParty[] { customer },
                        Resources = new ActivityParty[] { resource },
                    };
                    _repository.CreateCrmRecord(appointment);

                    Index();
                    strMsg = Properties.Settings.Default.AppointmentMsg;


                }
            }
            else //create a new CRM account
            {
                strMsg = Properties.Settings.Default.CRMAccountErrorMsg;

            }
            return Json(strMsg);
        }
        /// <summary>
        /// Converts a string into DateTime format
        /// </summary>
        /// <param name="date"></param>
        /// <returns>The parseDate. In case of error, DateTime.MinValue</returns>
        public static DateTime ConvertToDateTime(string date)
        {
            try
            {
                //string[] strArrDate = date.Split(' ');
                //date = strArrDate[0].TrimStart().TrimEnd().Trim();
                DateTime dt = DateTime.ParseExact(date, SUPPORTED_DATE_FORMATS, CultureInfo.InvariantCulture, DateTimeStyles.None);
                return dt;
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }
    }
}
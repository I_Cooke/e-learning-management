﻿using System.Data;
using Orchard.Data.Migration;

namespace DynamicsCRMWebPortal
{
    public class Migrations : DataMigrationImpl
    {
        public int Create()
        {
            // Creating table DynamicsCRMSettingsPartRecord
            SchemaBuilder.CreateTable("DynamicsCRMWebPortalSettingsPartRecord", table => table
                                                                                    .ContentPartRecord()
                                                                                    .Column("CRMConnectionString", DbType.String)
                );


            return 1;
        }
    }
}
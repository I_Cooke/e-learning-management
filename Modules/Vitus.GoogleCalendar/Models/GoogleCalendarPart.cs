﻿using Orchard.ContentManagement;
using System.ComponentModel.DataAnnotations;

namespace Vitus.GoogleCalendar.Models
{
    public class GoogleCalendarPart : ContentPart
    {
        [Required]
        public string GoogleCalendarApiKey
        {
            get { return this.Retrieve(x => x.GoogleCalendarApiKey); }
            set { this.Store(x => x.GoogleCalendarApiKey, value); }
        }

        [Required]
        public string GoogleCalendarIds
        {
            get { return this.Retrieve(x => x.GoogleCalendarIds); }
            set { this.Store(x => x.GoogleCalendarIds, value); }
        }

        public string GoogleCalendarClasses
        {
            get { return this.Retrieve(x => x.GoogleCalendarClasses); }
            set { this.Store(x => x.GoogleCalendarClasses, value); }
        }

        public bool Theme
        {
            get { return this.Retrieve(x => x.Theme); }
            set { this.Store(x => x.Theme, value); }
        }

        public FullCalendarDefaultView DefaultView
        {
            get { return this.Retrieve(x => x.DefaultView); }
            set { this.Store(x => x.DefaultView, value); }
        }

        public string HeaderLeft
        {
            get { return this.Retrieve(x => x.HeaderLeft); }
            set { this.Store(x => x.HeaderLeft, value); }
        }

        public string HeaderCenter
        {
            get { return this.Retrieve(x => x.HeaderCenter); }
            set { this.Store(x => x.HeaderCenter, value); }
        }

        public string HeaderRight
        {
            get { return this.Retrieve(x => x.HeaderRight); }
            set { this.Store(x => x.HeaderRight, value); }
        }

        public bool Weekends
        {
            get { return this.Retrieve(x => x.Weekends); }
            set { this.Store(x => x.Weekends, value); }
        }

        public bool WeekNumbers
        {
            get { return this.Retrieve(x => x.WeekNumbers); }
            set { this.Store(x => x.WeekNumbers, value); }
        }

        public bool AllDaySlot
        {
            get { return this.Retrieve(x => x.AllDaySlot); }
            set { this.Store(x => x.AllDaySlot, value); }
        }
        
        [Range(typeof(byte), "0", "23")]
        public byte MinTime
        {
            get { return (byte)this.Retrieve<int>("MinTime"); }
            set { this.Store<int>("MinTime", (int)value); }
        }

        [Range(typeof(byte), "1", "24")]
        public byte MaxTime
        {
            get { return (byte)this.Retrieve<int>("MaxTime"); }
            set { this.Store<int>("MaxTime", (int)value); }
        }
    }
}
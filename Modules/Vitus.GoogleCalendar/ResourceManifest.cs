using Orchard.UI.Resources;

namespace Vitus.GoogleCalendar
{
    public class ResourceManifest : IResourceManifestProvider
    {
        public void BuildManifests(ResourceManifestBuilder builder)
        {
            var manifest = builder.Add();

            manifest.DefineScript("Moment").SetUrl("moment.min.js").SetVersion("2.8.4");

            manifest.DefineScript("FullCalendar").SetUrl("fullcalendar.min.js", "fullcalendar.js").SetVersion("2.2.2")
                .SetDependencies("jQuery", "Moment");

            manifest.DefineScript("FullCalendar_GoogleCalendar").SetUrl("gcal.js").SetVersion("2.2.2")
                .SetDependencies("FullCalendar");

            manifest.DefineStyle("FullCalendar").SetUrl("fullcalendar.min.css", "fullcalendar.css").SetVersion("2.2.2");
        }
    }
}

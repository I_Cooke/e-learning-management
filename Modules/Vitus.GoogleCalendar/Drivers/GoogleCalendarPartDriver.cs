﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using System;
using Vitus.GoogleCalendar.Models;

namespace Vitus.GoogleCalendar.Drivers
{
    public class GoogleCalendarPartDriver : ContentPartDriver<GoogleCalendarPart>
    {
        protected override string Prefix { get { return "GoogleCalendar"; } }

        protected override DriverResult Display(GoogleCalendarPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_GoogleCalendar",
                () => shapeHelper.Parts_GoogleCalendar(Calendar: part));
        }

        protected override DriverResult Editor(GoogleCalendarPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_GoogleCalendar_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts/GoogleCalendar",
                    Model: part,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(GoogleCalendarPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);

            return Editor(part, shapeHelper);
        }

        protected override void Importing(GoogleCalendarPart part, ImportContentContext context)
        {
            var googleCalendarApiKey = context.Attribute(part.PartDefinition.Name, "GoogleCalendarApiKey");
            var googleCalendarIds = context.Attribute(part.PartDefinition.Name, "GoogleCalendarIds");
            var googleCalendarClasses = context.Attribute(part.PartDefinition.Name, "GoogleCalendarClasses");
            var theme = context.Attribute(part.PartDefinition.Name, "Theme");
            var defaultView = context.Attribute(part.PartDefinition.Name, "DefaultView");
            var headerLeft = context.Attribute(part.PartDefinition.Name, "HeaderLeft");
            var headerCenter = context.Attribute(part.PartDefinition.Name, "HeaderCenter");
            var headerRight = context.Attribute(part.PartDefinition.Name, "HeaderRight");
            var weekends = context.Attribute(part.PartDefinition.Name, "Weekends");
            var weekNumbers = context.Attribute(part.PartDefinition.Name, "WeekNumbers");
            var allDaySlot = context.Attribute(part.PartDefinition.Name, "AllDaySlot");
            var minTime = context.Attribute(part.PartDefinition.Name, "MinTime");
            var maxTime = context.Attribute(part.PartDefinition.Name, "MaxTime");

            if (googleCalendarApiKey != null) part.GoogleCalendarApiKey = googleCalendarApiKey;
            if (googleCalendarIds != null) part.GoogleCalendarIds = googleCalendarIds;
            if (googleCalendarClasses != null) part.GoogleCalendarClasses = googleCalendarClasses;
            if (theme != null) part.Theme = bool.Parse(theme);
            if (defaultView != null) part.DefaultView = (FullCalendarDefaultView)Enum.Parse(typeof(FullCalendarDefaultView), defaultView); ;
            if (headerLeft != null) part.HeaderLeft = headerLeft;
            if (headerCenter != null) part.HeaderCenter = headerCenter;
            if (headerRight != null) part.HeaderRight = headerRight;
            if (weekends != null) part.Weekends = bool.Parse(weekends);
            if (weekNumbers != null) part.WeekNumbers = bool.Parse(weekNumbers);
            if (allDaySlot != null) part.AllDaySlot = bool.Parse(allDaySlot);
            if (minTime != null) part.MinTime = byte.Parse(minTime);
            if (maxTime != null) part.MaxTime = byte.Parse(maxTime);
        }

        protected override void Exporting(GoogleCalendarPart part, ExportContentContext context)
        {
            context.Element(part.PartDefinition.Name).SetAttributeValue("GoogleCalendarApiKey", part.GoogleCalendarApiKey);
            context.Element(part.PartDefinition.Name).SetAttributeValue("GoogleCalendarIds", part.GoogleCalendarIds);
            context.Element(part.PartDefinition.Name).SetAttributeValue("GoogleCalendarClasses", part.GoogleCalendarClasses);
            context.Element(part.PartDefinition.Name).SetAttributeValue("Theme", part.Theme);
            context.Element(part.PartDefinition.Name).SetAttributeValue("DefaultView", part.DefaultView.ToString("G"));
            context.Element(part.PartDefinition.Name).SetAttributeValue("HeaderLeft", part.HeaderLeft);
            context.Element(part.PartDefinition.Name).SetAttributeValue("HeaderCenter", part.HeaderCenter);
            context.Element(part.PartDefinition.Name).SetAttributeValue("HeaderRight", part.HeaderRight);
            context.Element(part.PartDefinition.Name).SetAttributeValue("Weekends", part.Weekends);
            context.Element(part.PartDefinition.Name).SetAttributeValue("WeekNumbers", part.WeekNumbers);
            context.Element(part.PartDefinition.Name).SetAttributeValue("AllDaySlot", part.AllDaySlot);
            context.Element(part.PartDefinition.Name).SetAttributeValue("MinTime", part.MinTime);
            context.Element(part.PartDefinition.Name).SetAttributeValue("MaxTime", part.MaxTime);
        }
    }
}

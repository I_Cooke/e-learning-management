﻿using Orchard.ContentManagement;
using Orchard.Core.Common.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace dsc.CalendarWidget.Models
{
    public class CalendarEventDefinition : ContentPart<CalendarEventDefinitionRecord>
    {
        public string TimeZone {
            get { return Record.TimeZone; }
            set { Record.TimeZone = value; }
        }

        [Required]
        public DateTime? StartDateTime
        {
            get { return Record.StartDateTime; }
            set { Record.StartDateTime = value; }
        }

        [Required]
        public DateTime? EndDateTime
        {
            get { return Record.EndDateTime; }
            set { Record.EndDateTime = value; }
        }

        public bool IsAllDay
        {
            get { return Record.IsAllDay; }
            set { Record.IsAllDay = value; }
        }

        public string Url
        {
            get { return Record.Url; }
            set { Record.Url = value; }
        }

        public bool IsRecurring
        {
            get { return Record.IsRecurring; }
            set { Record.IsRecurring = value; }
        }

        //internal CalendarEventDefinitionPart Copy()
        //{
        //    return new CalendarEventDefinitionPart()
        //    {
        //        TimeZone = TimeZone,
        //        StartDateTime = StartDateTime.GetValueOrDefault(),
        //        EndDateTime = EndDateTime.GetValueOrDefault(),
        //        IsAllDay = IsAllDay,
        //        Url = Url,
        //        IsRecurring = IsRecurring
        //    };
        //}
    }
}

#region

using System.Data;
using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;

#endregion

namespace DueMetri.Activities.Migrations
{
    public class ActivityMigrations : DataMigrationImpl
    {
        public int Create()
        {
            // Creating table ActivityPartRecord
            SchemaBuilder.CreateTable(
                "ActivityPartRecord",
                table => table
                             .ContentPartRecord()
                             .Column("Title", DbType.String)
                             .Column("DateTimeStart", DbType.DateTime)
                             .Column("DateTimeEnd", DbType.DateTime)
                             .Column("Url", DbType.String)
                             .Column("Category", DbType.String)
                );


            return 1;
        }

        public int UpdateFrom1()
        {
            ContentDefinitionManager.AlterTypeDefinition(
                "Activity",
                cfg => cfg
                           .WithPart("ActivityPart")
                           .WithPart("CommonPart")
                           .Creatable(false)
                );

            return 4;
        }

        public int UpdateFrom4()
        {
            ContentDefinitionManager.AlterPartDefinition(
                "CalendarPart",
                builder => builder.Attachable());

            ContentDefinitionManager.AlterTypeDefinition(
                "ActivityCalendar",
                cfg => cfg
                           .WithPart("TitlePart")
                           .WithPart("AutoroutePart",
                                     builder => builder
                                                    .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                                                    .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                                                    .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name:'Title', Pattern: 'calendar/{Content.Slug}', Description: 'Calendar'}]")
                                                    .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                           .WithPart("CalendarPart")
                           .WithPart("CommonPart")
                           .Creatable()
                );

            return 6;
        }

        public int UpdateFrom6()
        {
            SchemaBuilder.AlterTable(
                "ActivityPartRecord",
                table => table
                             .AddColumn("ShowInHome", DbType.Boolean, x => x.WithDefault(false))
                );

            return 7;
        }
    }
}

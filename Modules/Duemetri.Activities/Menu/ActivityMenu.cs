﻿#region

using DueMetri.Activities.Permissions;
using Orchard;
using Orchard.Localization;
using Orchard.Security;
using Orchard.UI.Navigation;

#endregion

namespace DueMetri.Activities.Menu
{
    public class ActivityMenu : INavigationProvider
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IWorkContextAccessor _workContextAccessor;

        public ActivityMenu(IAuthorizationService authorizationService, IWorkContextAccessor workContextAccessor)
        {
            _authorizationService = authorizationService;
            _workContextAccessor = workContextAccessor;
        }

        public Localizer T { get; set; }

        #region INavigationProvider Members

        public string MenuName
        {
            get { return "admin"; }
        }

        public void GetNavigation(NavigationBuilder builder)
        {
            builder.AddImageSet("activities")
                   .Add(T("Activities"), "1.6", BuildMenu);
        }

        #endregion

        private void BuildMenu(NavigationItemBuilder menu)
        {
            menu.Add(T("List"), "2",
                     item => item.Action("List", "Activity", new{area = "DueMetri.Activities"})
                                 .Permission(ActivityPermissions.ViewActivity));

            menu.Add(T("Add new"), "4",
                     item => item.Action("Create", "Activity", new{area = "DueMetri.Activities"})
                                 .Permission(ActivityPermissions.ManageActivity));
        }
    }
}

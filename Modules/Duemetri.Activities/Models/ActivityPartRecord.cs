﻿#region

using System;
using Orchard.ContentManagement.Records;

#endregion

namespace DueMetri.Activities.Models
{
    public class ActivityPartRecord : ContentPartRecord
    {
        public virtual string Title { get; set; }
        public virtual DateTime DateTimeStart { get; set; }
        public virtual DateTime DateTimeEnd { get; set; }
        public virtual string Url { get; set; }
        public virtual string Category { get; set; }
        public virtual bool ShowInHome { get; set; }
    }
}

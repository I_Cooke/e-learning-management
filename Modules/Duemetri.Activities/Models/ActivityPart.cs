﻿#region

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;

#endregion

namespace DueMetri.Activities.Models
{
    public class ActivityPart : ContentPart<ActivityPartRecord>, ITitleAspect
    {
        [DataType(DataType.DateTime)]
        [Range(typeof (DateTime), "1-Jan-1900", "1-Jan-2060")]
        public DateTime DateTimeStart
        {
            get { return Record.DateTimeStart; }
            set { Record.DateTimeStart = value; }
        }

        [DataType(DataType.Date)]
        [Range(typeof (DateTime), "1-Jan-1900", "1-Jan-2060")]
        public DateTime DateTimeEnd
        {
            get { return Record.DateTimeEnd; }
            set { Record.DateTimeEnd = value; }
        }

        [StringLength(180)]
        public string Url
        {
            get { return Record.Url; }
            set { Record.Url = value; }
        }

        [StringLength(180)]
        public string Category
        {
            get { return Record.Category; }
            set { Record.Category = value; }
        }

        public bool ShowInHome
        {
            get { return Record.ShowInHome; }
            set { Record.ShowInHome = value; }
        }

        #region ITitleAspect Members

        [StringLength(180)]
        public string Title
        {
            get { return Record.Title; }
            set { Record.Title = value; }
        }

        #endregion
    }
}

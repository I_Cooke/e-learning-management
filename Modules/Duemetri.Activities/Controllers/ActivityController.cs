﻿#region

using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using DueMetri.Activities.Models;
using DueMetri.Activities.Permissions;
using DueMetri.Activities.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Localization.Services;
using Orchard.Settings;
using Orchard.UI.Admin;
using Orchard.UI.Navigation;
using Orchard.UI.Notify;

#endregion

namespace DueMetri.Activities.Controllers
{
    [ValidateInput(false)]
    public class ActivityController : Controller, IUpdateModel
    {
        private readonly IContentManager _contentManager;
        private readonly IOrchardServices _services;
        private readonly ISiteService _siteService;

        public ActivityController(
            ISiteService siteService,
            IOrchardServices services,
            IShapeFactory shapeFactory,
            IContentManager contentManager)
        {
            _siteService = siteService;
            _services = services;
            Shape = shapeFactory;
            _contentManager = contentManager;
            T = NullLocalizer.Instance;

        }

        private dynamic Shape { get; set; }
        public Localizer T { get; set; }

        #region IUpdateModel Members

        bool IUpdateModel.TryUpdateModel<TModel>(TModel model, string prefix, string[] includeProperties, string[] excludeProperties)
        {
            return TryUpdateModel(model, prefix, includeProperties, excludeProperties);
        }

        public void AddModelError(string key, LocalizedString errorMessage)
        {
            ModelState.AddModelError(key, errorMessage.ToString());
        }

        #endregion

        [Admin]
        public ActionResult List(PagerParameters pagerParameters, string sortOrder)
        {
            sortOrder = String.IsNullOrEmpty(sortOrder) ? string.Empty : sortOrder.ToLowerInvariant();
            ViewBag.FromSortParm = String.IsNullOrEmpty(sortOrder) ? "from_desc" : "";
            ViewBag.ToSortParm = sortOrder == "to" ? "to_desc" : "to";
            ViewBag.TitleSortParm = sortOrder == "title" ? "title_desc" : "title";
            ViewBag.ShowInHomeParm = sortOrder == "showhome" ? "showhome_desc" : "showhome";

            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters.Page, pagerParameters.PageSize);
            var featuredContent = _contentManager.Query<ActivityPart, ActivityPartRecord>().List().ToList();

            switch (sortOrder.ToLowerInvariant()) {
                case "title":
                    featuredContent = featuredContent.OrderBy(s => s.Title).ToList();
                    break;
                case "title_desc":
                    featuredContent = featuredContent.OrderByDescending(s => s.Title).ToList();
                    break;
                case "showhome":
                    featuredContent = featuredContent.OrderBy(s => s.ShowInHome).ThenBy(s => s.DateTimeStart).ToList();
                    break;
                case "showhome_desc":
                    featuredContent = featuredContent.OrderByDescending(s => s.ShowInHome).ThenBy(s => s.DateTimeStart).ToList();
                    break;
                case "end":
                    featuredContent = featuredContent.OrderBy(s => s.DateTimeEnd).ToList();
                    break;
                case "end_desc":
                    featuredContent = featuredContent.OrderByDescending(s => s.DateTimeEnd).ToList();
                    break;
                case "from_desc":
                    featuredContent = featuredContent.OrderByDescending(s => s.DateTimeStart).ToList();
                    break;
                case "":
                    featuredContent = featuredContent.OrderBy(s => s.DateTimeStart).ToList();
                    break;
            }

            var paginatedContent = featuredContent
                .Skip(pager.GetStartIndex())
                .Take(pager.PageSize)
                .ToList();

            var pagerShape = Shape.Pager(pager).TotalItemCount(featuredContent.Count());
            var vm = new ActivityIndexViewModel{
                Activities = paginatedContent,
                Pager = pagerShape
            };
            return View(vm);
        }

        [Admin]
        public ActionResult Create()
        {
            if (!_services.Authorizer.Authorize(ActivityPermissions.ManageActivity, T("Not authorized"))) {
                return new HttpUnauthorizedResult();
            }

            var activity = _services.ContentManager.New("Activity");
            if (activity == null) {
                return HttpNotFound();
            }

            var model = _services.ContentManager.BuildEditor(activity);
            // Casting to avoid invalid (under medium trust) reflection over the protected View method and force a static invocation.
            return View((object) model);
        }

        [HttpPost, ActionName("Create")]
        [Admin]
        public ActionResult CreatePOST()
        {
            if (!_services.Authorizer.Authorize(ActivityPermissions.ManageActivity, T("Not authorized"))) {
                return new HttpUnauthorizedResult();
            }

            var activity = _services.ContentManager.New("Activity");
            _contentManager.Create(activity);
            var model = _services.ContentManager.UpdateEditor(activity, this);

            if (!ModelState.IsValid) {
                _services.TransactionManager.Cancel();

                // Casting to avoid invalid (under medium trust) reflection over the protected View method and force a static invocation.
                return View((object) model);
            }

            _services.Notifier.Information(T("Activity succesfully created"));
            return RedirectToAction("List");
        }

        public ActionResult GetActivities(double start, double end)
        {
            var startDate = UnixTimeStampToDateTime(start);
            var endDate = UnixTimeStampToDateTime(end);

            var activities = _contentManager.Query<ActivityPart, ActivityPartRecord>()
                                            .Where(x => (startDate <= x.DateTimeStart && x.DateTimeStart <= endDate) ||
                                                        (startDate <= x.DateTimeEnd && x.DateTimeEnd <= endDate)).OrderBy(x => x.DateTimeStart).List();
            var selected = activities.Select(x => new ActivityJsonViewModel{
                id = x.Id,
                start = x.DateTimeStart.ToString("yyyy-MM-dd"),
                end = x.DateTimeEnd.Year > 1900 ? x.DateTimeEnd.ToString("yyyy-MM-dd") : x.DateTimeStart.ToString("yyyy-MM-dd"),
                title = x.Title,
                url = x.Url,
                backgroundColor = x.Category
            });

            return Json(selected, JsonRequestBehavior.AllowGet);
        }

        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}

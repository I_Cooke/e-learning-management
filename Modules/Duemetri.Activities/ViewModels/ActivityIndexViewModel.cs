﻿#region

using System.Collections.Generic;
using DueMetri.Activities.Models;

#endregion

namespace DueMetri.Activities.ViewModels
{
    public class ActivityIndexViewModel
    {
        public IEnumerable<ActivityPart> Activities { get; set; }
        public dynamic Pager { get; set; }
    }
}

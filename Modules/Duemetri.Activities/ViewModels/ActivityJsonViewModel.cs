﻿#region



#endregion

namespace DueMetri.Activities.ViewModels
{
    public class ActivityJsonViewModel
    {
        private string _backgroundColor;
        // ReSharper disable InconsistentNaming
        public int id { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string url { get; set; }

        public string backgroundColor
        {
            get { return _backgroundColor; }
            set
            {
                if (!string.IsNullOrEmpty(value) && value.Length == 6) {
                    _backgroundColor = "#" + value;
                }
            }
        }

        // ReSharper restore InconsistentNaming
    }
}

﻿#region

using System.Collections.Generic;
using Orchard.Environment.Extensions.Models;
using Orchard.Security.Permissions;

#endregion

namespace DueMetri.Activities.Permissions
{
    public class ActivityPermissions : IPermissionProvider
    {
        public static readonly Permission ViewActivity = new Permission{Description = "View ", Name = "ViewActivity"};
        public static readonly Permission ManageActivity = new Permission{Description = "Manage Activities", Name = "ManageActivity"};

        #region IPermissionProvider Members

        public virtual Feature Feature { get; set; }

        public IEnumerable<Permission> GetPermissions()
        {
            return new[]{
                ViewActivity,
                ManageActivity,
            };
        }

        public IEnumerable<PermissionStereotype> GetDefaultStereotypes()
        {
            return new[]{
                new PermissionStereotype{
                    Name = "Administrator",
                    Permissions = new[]{ViewActivity, ManageActivity}
                },
                new PermissionStereotype{
                    Name = "Anonymous",
                },
                new PermissionStereotype{
                    Name = "Authenticated",
                },
                new PermissionStereotype{
                    Name = "Editor",
                    Permissions = new[]{ViewActivity, ManageActivity}
                },
                new PermissionStereotype{
                    Name = "Moderator",
                },
                new PermissionStereotype{
                    Name = "Author",
                    Permissions = new[]{ViewActivity, ManageActivity}
                },
                new PermissionStereotype{
                    Name = "Contributor",
                    Permissions = new[]{ViewActivity}
                },
            };
        }

        #endregion
    }
}

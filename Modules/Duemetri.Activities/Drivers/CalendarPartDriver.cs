﻿#region

using DueMetri.Activities.Models;
using Orchard.ContentManagement.Drivers;
using Orchard.Localization;

#endregion

namespace DueMetri.Activities.Drivers
{
    /// <summary>
    /// Driver for the CalendarPart.
    /// Think about drivers as controllers for your parts. They are responsible for UI (display/edit your part).
    /// </summary>
    public class CalendarPartDriver : ContentPartDriver<CalendarPart>
    {
        public CalendarPartDriver()
        {
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        /// <summary>
        /// This method is responsible for displaying your part in the frontend.
        /// </summary>
        /// <param name="part">Your part.</param>
        /// <param name="displayType">The display type.</param>
        /// <param name="shapeHelper">The shape helper.</param>
        /// <returns></returns>
        protected override DriverResult Display(CalendarPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_Calendar",
                                () => shapeHelper.Parts_Calendar(ContentPart: part));
        }

        // There is nothing to edit and update, so we don't need Editor methods.
    }
}

﻿#region

using System;
using System.Globalization;
using DueMetri.Activities.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Localization;

#endregion

namespace DueMetri.Activities.Drivers
{
    public class ActivityPartDriverDriver : ContentPartDriver<ActivityPart>
    {
        private readonly Lazy<CultureInfo> _cultureInfo;

        public ActivityPartDriverDriver(
            IOrchardServices services)
        {
            T = NullLocalizer.Instance;
            Services = services;

            // initializing the culture info lazy initializer
            _cultureInfo = new Lazy<CultureInfo>(() => CultureInfo.GetCultureInfo(Services.WorkContext.CurrentCulture));
        }

        public Localizer T { get; set; }
        public IOrchardServices Services { get; set; }

        protected override string Prefix
        {
            get { return "Activity"; }
        }

        protected override DriverResult Display(ActivityPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape(
                "Parts_Activity",
                () => shapeHelper.Parts_Activity(
                    Title: part.Title,
                    DateTimeStart: part.DateTimeStart,
                    DateTimeEnd: part.DateTimeEnd,
                    Url: part.Url,
                    Category: part.Category));
        }

        //GET
        protected override DriverResult Editor(ActivityPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_Activity_Edit",
                                () => shapeHelper.EditorTemplate(
                                    TemplateName: "Parts/Activity",
                                    CultureInfo: _cultureInfo,
                                    Model: part,
                                    Prefix: Prefix));
        }

        //POST
        protected override DriverResult Editor(ActivityPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }
    }
}

﻿#region

using System;
using DueMetri.Activities.Models;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;

#endregion

namespace DueMetri.Activities.Handlers
{
    public class ActivityPartHandler : ContentHandler
    {
        public ActivityPartHandler(IRepository<ActivityPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));

            OnInitializing<ActivityPart>((context, part) =>{
                part.DateTimeStart = new DateTime(1900, 1, 1);
                part.DateTimeEnd = new DateTime(1900, 1, 1);
            });
        }
    }
}
